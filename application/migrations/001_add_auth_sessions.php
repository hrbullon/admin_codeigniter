<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_auth_sessions extends CI_Migration {

	public function up() {
		$this->dbforge->add_field(array(
                'id' => array(
                        'type' => 'VARCHAR',
                        'constraint' => 128,
                        'null' => FALSE
                ),
                'ip_address' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '45',
                        'null' => FALSE
                ),
                'timestamp' => array(
                        'type' => 'INT',
                        'constraint' => '10',
                        'unsigned' => TRUE,
                        'null' => FALSE
                ),
                'data' => array(
                        'type' => 'BLOB',
                        'null' => FALSE
                ),
        ));
        $this->dbforge->add_key('timestamp', TRUE);
        $this->dbforge->create_table('auth_sessions');
	}

	public function down() {
		$this->dbforge->drop_table('auth_sessions');
	}

}

/* End of file 001_add_auth_sessions.php */
/* Location: ./application/migrations/001_add_auth_sessions.php */