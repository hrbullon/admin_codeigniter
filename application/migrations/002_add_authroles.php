class <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_authroles extends CI_Migration {

	public function up() {
		$this->dbforge->add_field(array(
                'id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'auto_increment' => TRUE,
                        'null' => FALSE
                ),
                'name' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '45',
                        'null' => FALSE
                ),
                'state' => array(
                        'type' => 'INT',
                        'null' => FALSE
                ),
                'deleted' => array(
                        'type' => 'INT',
                        'null' => FALSE
                ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('auth_roles2');
	}

	public function down() {
		$this->dbforge->drop_table('auth_roles2');
	}

}

/* End of file 001_add_authroles.php */
/* Location: ./application/migrations/001_add_authroles.php */