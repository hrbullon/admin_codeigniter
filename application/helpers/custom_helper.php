<?php 

if (!defined('BASEPATH'))
    exit('No estan permitidos los scripts directos');

if (! function_exists('generate_code'))
{
    function generate_code($lengh) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $lengh;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
}

if (! function_exists('send_email'))
{
    function send_email($address,$subject,$message,$attach = false)
    {
        $ci = get_instance();

        $ci->load->model('Settings_model','settings');
        $ci->load->model('Users_model','users');

        if(isset($ci->session->company)){
            $model = $ci->settings->get_data($ci->session->company);
        }else{
            $user  = $ci->users->get_user_by_username($address);
            $model = $ci->settings->get_data($user->company);
        }

        if($model){
            //Inicializo las variables de conexión con el servidor de correo
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $model->outserver,
                'smtp_port' => $model->outport,
                'smtp_user' => $model->out_username,
                'smtp_pass' => $model->out_password,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n"
            );

            $ci->email->initialize($config);
            $ci->email->from($model->out_username, $model->out_name);
            $ci->email->to($address);
            $ci->email->subject($subject);
            $ci->email->message($message);
            
            if($attach){
                $ci->email->attach($attach);
            }
            
            return ($ci->email->send())? true : false;

        }else{
            return false;
        }
    }
}

if (! function_exists('get_menu_admin'))
{
    function get_menu_admin()
    {
        $ci =& get_instance();
        $ci->load->model('Menus_model','menus');

        $parents = $ci->menus->get_parent_menus();

        if(count($parents) > 0){

            foreach ($parents as $value) {
                
                $children = $ci->menus->get_children_menus($value->id);

                if(count($children) > 0){

                    echo '<li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="'.$value->icon.'"></i>
                        <span>'.$value->name.' </span> 
                    </a>';   
                
                }else{
                     echo '<li class="">
                        <a class="hash" description="'.$value->route.'" href="'.base_url("app#".$value->route).'">
                            <i class="'.$value->icon.'"></i>
                            <span>'.$value->name.' </span> 
                        </a>';
                }

                if(count($children) > 0){
                    echo '  <ul class="sub-menu">';

                    foreach ($children as $child) {

                        //Verifico si el usuari o tiene pemrisos para 
                        if(check_permission((int)$child->permission)){
                            echo '<li>
                                <a class="hash" description="'.$child->route.'" href="'.base_url("app#".$child->route).'">
                                    '.$child->name.'
                                </a>
                             </li>';    
                        }
                        
                    }

                    echo '  </ul>';
                }
                
                echo '</li>';
            }
        }
    } 
}


if ( ! function_exists('set_config_item'))
{
    function set_config_item($file,$item,$value)
    {   
        if($file == "1")
        {
            $archivo = APPPATH. 'config/config.php';
        }else{
            $archivo = APPPATH. 'config/custom.php';
        }
        
        $contenido = file_get_contents($archivo);
        $lineas = explode("\n", $contenido);
        
        for($i=0; $i<count($lineas); $i++){

           if(preg_match('/config/', $lineas[$i]) && preg_match("/$item/", $lineas[$i])){
               
                $edit = "config['$item'] = '".$value."';";
                file_put_contents($archivo, str_replace($lineas[$i], "$".$edit, $contenido));
           } 
        }
    }
}

if ( ! function_exists('get_settings_company'))
{
    function get_settings_company()
    {
        $ci =& get_instance();

        $ci->db->where('company',$ci->session->company);
        return $ci->db->get('settings')->row();
    }
}

if ( ! function_exists('get_formats_date'))
{
    function get_formats_date()
    {
        $ci =& get_instance();

        return $ci->db->get('formats_date')->result();
    }
}

if ( ! function_exists('get_time_zones'))
{
    function get_time_zones()
    {
        $ci =& get_instance();

        return $ci->db->get('time_zones')->result();
    }
}

if (! function_exists('check_permission'))
{
    function check_permission($permission)
    {
        $ci =& get_instance();
        $ci->load->model('Privileges_model','privileges');
    
        return $ci->privileges->check_permission($ci->session->userdata('user_id'),$permission);
    }
}

if (! function_exists('check_activated_menu'))
{
    function check_activated_menu($menu)
    {
        $ci =& get_instance();
        $ci->load->model('Menus_model','menus');
        
        if(!$ci->menus->check_activated_menu($menu)){
            redirect('errors/desactivated');
        }
    }
}

if (! function_exists('get_real_ip'))
{
    function get_real_ip()
    {

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
            return $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
            return $_SERVER["REMOTE_ADDR"];
        }

    }
}   

if (! function_exists('open_cypher'))
{
    function open_cypher($action='encrypt',$string=false)
    {
        $action = trim($action);
        $output = false;

        $myKey = SALT_HASH;
        $myIV = FIRST_HASH;
        $encrypt_method = 'AES-256-CBC';

        $secret_key = hash('sha256',$myKey);
        $secret_iv = substr(hash('sha256',$myIV),0,16);

        if ( $action && ($action == 'encrypt' || $action == 'decrypt') && $string )
        {
            $string = trim(strval($string));

            if ( $action == 'encrypt' )
            {
                $output = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
                $output = str_replace('/','XTS',$output);
                $output = urlencode($output);
            };

            if ( $action == 'decrypt' )
            {
                $string = urldecode($string);
                $string = str_replace('XTS','/',$string);
                $string = trim(strval($string));
                $output = openssl_decrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
            };
        };

        return $output;
    }
}