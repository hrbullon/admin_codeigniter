<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  auth_permissions
 *  
 * ---------------Atrributes---------------
    id int
    name  string 
    description string
    parent tynint (If some permission depends of another, then here should be that id)
 **/

class Privileges_model extends CI_Model {    
    
    private $table = "auth_permissions";
    private $column_order = array(null, 'name','description'); 
    private $column_search = array('name','description');    	
	private $order = array('id' => 'asc');

    public function __construct() {
        parent::__construct();
    }	

    /**
     * @param permission string  
     * @return Object (Info about permission)
     **/
    public function get_data($permission)
    {
        $this->db->where('name',$permission);
        return $this->db->get('auth_permissions')->row();
    }

    /**
     * @param role string  
     * @return Array (List about permissions in a determinated role)
     **/
    public function get_all_privileges($role)
    {	
    	$this->db->where('deleted',0);
    	$this->db->where('name',$role);
    	$role_model = $this->db->get('auth_roles')->row();

    	if ($role_model) {
    		$this->db->select('auth_permissions.name,auth_roles_permissions.permission,
    			auth_roles_permissions.state');
    		$this->db->from('auth_roles_permissions');
    		$this->db->join("auth_permissions","auth_permissions.id=auth_roles_permissions.permission");
    		$this->db->where('role',$role_model->id);
    		$this->db->where('state',1);

    		return $this->db->get()->result();
    	}else{
    		return false;
    	}
    }

    /**
     *
     * Check if user or role has some permission in the system 
     * 
     * @param user int
     * @param permission string 
     * @return Boolean
     **/
    public function check_permission($user,$permission)
    {

        $this->db->where('deleted',0);
        $this->db->where('user',$user);
        
        $roles = $this->db->get('auth_user_roles')->result();
        
        if ($roles) {

            foreach ($roles as $value) {

                $this->db->where('auth_roles_permissions.state',1);
                $this->db->where('auth_roles_permissions.role',$value->role);

                if(is_int($permission)){
                    $this->db->where('auth_roles_permissions.permission',$permission);
                }else{
                    $this->db->where('auth_permissions.name',$permission);
                }

                $this->db->join('auth_permissions', 'auth_permissions.id = auth_roles_permissions.permission');

                $permission = $this->db->get('auth_roles_permissions')->row();              
                
                if($permission){
                    return true;
                }
            }

        }
    }

    /**
     * @return Array (List about permissions)
     **/
    public function get_all()
    {
        return $this->db->get('auth_permissions')->result();
    }

    /**
     *
     * Save object about permission iun the system
     * 
     * @param role int  
     * @param action string
     * @param permission string    
     * @return Array (List of roles for one user)
     **/
    public function asign_revoke($role,$action,$permissions)
    {
    	//Start transaction
        $this->db->trans_begin();

        $this->db->where('name',$role);
    	$role_model = $this->db->get('auth_roles')->row();

        foreach ($permissions as $value) {
        	//Obtengo los datos del permiso recibido
        	$this->db->where('name',$value);
    		$permission_model = $this->db->get('auth_permissions')->row();
			
			if($role_model && $permission_model){

	    		$this->db->where('role',$role_model->id);
		    	$this->db->where('permission',$permission_model->id);
		    	$item = $this->db->get('auth_roles_permissions')->row();

		    	$data['role'] = $role_model->id;
		    	$data['permission'] = $permission_model->id;

		    	if ($item) {
		    		//Está activo entonces lo inactivo, sino lo activo
		    		$data['state'] = $action;

		    		$this->db->where('role',$role_model->id);
		    		$this->db->where('permission',$permission_model->id);
		    		$this->db->update('auth_roles_permissions',$data);
		    	}else{
		    		$data['state'] = 1;
		    		$this->db->insert('auth_roles_permissions',$data);
		    	}

		    }else{
	    		return false;
	    	}
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return true;
        }
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('id,name,description');
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/

    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            
            //Privileges and Permissions
            //Assign permission
            case 1:

                $data['action'] = "Assign permission";
                $data['table']  = "roles_permissions";
                $data['description'] = "Asignación de permisos ".$model_decode->permission." al rol: ". $model_decode->role;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Revoke permission
            case 2:

                $data['action'] = "Revoke permission";
                $data['table']  = "roles_permissions";
                $data['description'] = "Remover permiso de ".$model_decode->permission." al rol: ". $model_decode->role;
                $data['time'] = date('Y-m-d H:i:s');

                break;
        }

        $data['model'] = $model;

        return  $data;

    }

}


