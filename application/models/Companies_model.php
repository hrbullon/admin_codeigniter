<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  companies
 * 
 * ---------------Atrributes---------------
    id int
    social  string (Name)
    fiscal string (RIF)
    email string
    local_phone string
    local_mobile string
    country int (id of country ej -> 1 = Venezuela)
    city string (name of city, departament, state and other)
    state tinyint (0 -> Inactivo, 1 -> Activo)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
 **/

class Companies_model extends CI_Model {    
	
    var $table = 'companies';
    var $column_order = array(null, 'social','fiscal','local_phone','local_mobile','state'); 
    var $column_search = array('social','fiscal','local_phone','local_mobile','state');    
    var $order = array('id','desc');

    public function __construct() {
		parent::__construct();
    }

    /**
     * @param id int  
     * @return Object (Info about determined company)
     **/
    public function get_data($id)
    {
        $this->db->where('id',$id);
        return $this->db->get($this->table)->row();
    }

    /**
     *
     * Save object in the table
     * 
     * @param data array('social','fiscal','email','local_phone','local_mobile','country','city','address','state')
     * @param id int  id of determinated company
     * @return Boolean 
     **/
    public function save($data,$id = null)
    {
        //Start transaction
        $this->db->trans_begin();
        if(!$id)
        {
            $data['deleted'] = 0;
            $id = ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;
        }else{
            $this->db->where(array('id'=>$id));
            $this->db->update($this->table,$data);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return true;
        }

    }

    /**
     *
     * Logical delete one item selected
     * 
     * @param id int  id of determinated company
     * @return Boolean 
     **/
    public function delete($id)
    {   
        $data['deleted'] = 1;
        $this->db->where('id', $id);
        return $this->db->update($this->table,$data);
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('id,social,fiscal,local_phone,local_mobile,state');
        $this->db->from($this->table);
        $this->db->where('deleted','0');
        
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by('id', 'desc');
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/

    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            //Companies table
            //Create company
            case 1:

                $data['action'] = "Create";
                $data['table']  = "companies";
                $data['object'] = $object;
                $data['description'] = " Creación de una nueva empresa: ". $model_decode->social;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Update company
            case 2:

                $data['action'] = "Update";
                $data['table']  = "companies";
                $data['object'] = $object;
                $data['description'] = "Actualización de la empresa:  ". $model_decode->social;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Delete company
            case 3:

                $data['action'] = "Delete";
                $data['table']  = "companies";
                $data['object'] = $object;
                $data['description'] = "Eliminación de la empresa:  ". $model_decode->social;
                $data['time'] = date('Y-m-d H:i:s');

                break;  
        }

        $data['model'] = $model;

        return  $data;

    }

}


