<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  auth_menu
 * 
 * ---------------Atrributes---------------
    id int
    name   string (Name or description about menu)
    parent  int (Id about other menu if it has a menu parent)
    position  int (Number of position in menu admin)
    route  string (path or url of menu)
    permission int (Id about required permission for open menu)
    icon string (name of icon fa awesome)
    state tinyint (0 -> Inactivo, 1 -> Activo)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
    
 **/

class Menus_model extends CI_Model {    
    
    var $table = 'auth_menu';
    var $column_order = array(null, 'name','state'); 
    var $column_search = array('name','state');    
    var $order = array('id','desc');

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param id int  
     * @return Object (Info about determined a menu)
     **/
    public function get_data($id)
    {   
        $this->db->where('id',$id);
        return $this->db->get($this->table)->row();
    }

    /**
     * @return Array (List all menues)
     **/
    public function get_all()
    {
        $this->db->where('deleted','0');
        $this->db->where('state','1');
        return $this->db->get($this->table)->result();
    }

    /**
     * @return Array (List all menues with children)
     **/
    public function get_parent_menus()
    {
        $this->db->where('parent is null ');
        $this->db->where('state','1');
        $this->db->where('deleted','0');
        $this->db->order_by('position','asc');
        return $this->db->get($this->table)->result();
    }

    /**
     * @return Array (List all menues children of determinated parent)
     **/
    public function get_children_menus($parent)
    {
        $this->db->where('parent',$parent);
        $this->db->where('state','1');
        $this->db->where('deleted','0');
        return $this->db->get($this->table)->result();
    }


    /**
     * Check if user or role have permission for a determinated menu
     * @param menu string (name of menu)
     * @return Boolean
     **/
    public function check_activated_menu($menu)
    {
        $this->db->where('name',$menu);
        $this->db->where('deleted','0');
        
        $menu = $this->db->get($this->table)->row();
        return ($menu->state == '1')? true : false;
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('deleted','0');
        
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by('id', 'desc');
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function delete($id)
    {   
        $data['deleted'] = 1;
        $this->db->where('id', $id);
        return $this->db->update($this->table,$data);
    }

    public function save($data,$id = null)
    {
        //Start transaction
        $this->db->trans_begin();
        if(!$id)
        {
            $data['deleted'] = 0;
            $id = ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;
        }else{
            $this->db->where(array('id'=>$id));
            $this->db->update($this->table,$data);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return true;
        }

    }

    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/

    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            //Menu table
            //Create 
            case 1:

                $data['action'] = "Create";
                $data['table']  = "menu";
                $data['description'] = "Creación de un nuevo menú: ".$model_decode->name;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            case 2:

                $data['action'] = "Update";
                $data['table']  = "menu";
                $data['description'] = "Actualización del menú: ".$model_decode->name;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            case 3:

                $data['action'] = "Delete";
                $data['table']  = "menu";
                $data['description'] = "Eliminación del menú: ".$model_decode->name;
                $data['time'] = date('Y-m-d H:i:s');

                break;

        }

        $data['model'] = $model;

        return  $data;

    }
}


