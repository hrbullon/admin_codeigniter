<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Codes_model extends CI_Model {    
   
	var $table = 'auth_codes';

    public function __construct() {
		parent::__construct();
    }

    public function get_code($code)
    {
    	$this->db->where(array('code' => $code));
    	return $this->db->get($this->table)->row();
    }

    public function insert($data)
    {
    	return ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;
    }

    public function update($data,$id)
    {
    	$this->db->where(array('id'=>$id));
		return $this->db->update($this->table,$data);
    }


}