<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  auth_sessions
 * 
 * ---------------Atrributes---------------
    id int
    profile  string (path to image profile)
    username string
    password string
    state tinyint (0 -> Inactivo, 1 -> Activo)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
 **/

class Sessions_model extends CI_Model {    
    
    private $table = 'sessions';
    
    private $column_order = array(null, 'auth_users.username','ip_address','start_time','end_time','sessions.state'); 

    private $column_search = array('auth_users.username','ip_address','start_time','end_time','sessions.state');      
    
    private $order = array('id' => 'desc');

    public function __construct() {
        parent::__construct();
    }


    /**
     *
     * Save object in the table
     * 
     * @param data array('user','ip_address','start_time','end_time','state')
     * @return Boolean 
     **/
    public function insert($data){
        return ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;
    }

     /**
     *
     * Save object about finnish of session in the table
     * 
     * @param  session int (Identificatior about session table)
     * @return Boolean 
     **/
    public function finnish($session){

        $data['state'] = '0';
        $data['end_time'] = date('Y-m-d H:i:s');

        $this->db->where('id',$session);
        return $this->db->update($this->table,$data);
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('sessions.id,auth_users.username,ip_address,start_time,end_time,sessions.state');
        $this->db->join('auth_users','auth_users.id = sessions.user');
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table." u");
        return $this->db->count_all_results();
    }

    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/

    //Get lastes sessions of user logued
    public function get_lastest_sessions()
    {
        $this->db->select('ip_address,start_time,end_time,sessions.state');
        $this->db->from($this->table);
        $this->db->limit(30);
        $this->db->order_by('id','desc');
        $this->db->where('user',$this->session->user_id);
        return $this->db->get()->result();
    }

    //Check session active in table of database
    public function check_active_session($session)
    {
        $this->db->where('id',$session);
        $this->db->where('state','0');
        $row = $this->db->get($this->table)->num_rows();
        
        return ($row > 0)? true:false;
    }

    public function destroy_all_sessions()
    {   
        $this->db->where("state",1);
        $this->db->where("id !=".$this->session->session_id);
        $this->db->where("user",$this->session->user_id);
        $sessions = $this->db->get($this->table)->result();

        //Are there active sessions
        if( count($sessions) > 0 ){
            $data['state'] = 0;

            //Update rows in sessions to close
            $this->db->where("state",1);
            $this->db->where("id !=".$this->session->session_id);
            $this->db->where("user",$this->session->user_id);
            return $this->db->update($this->table, $data);
        }else{
            return false;
        }
    }

    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            //Destroy all sessions
            case 1:

                $data['action'] = "Destroy";
                $data['table']  = "sessions";
                $data['object'] = $this->session->session_id;
                $data['description'] = "Cierre de todas las sesiones activas";
                $data['time'] = date('Y-m-d H:i:s');

                break;
        }

        $data['model'] = $model;

        return  $data;

    }
}


