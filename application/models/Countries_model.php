<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  countries
 * 
 * ---------------Atrributes---------------
    id int
    code  smallint 
    iso3166a1 string(2)
    iso3166a2 string(4)
    name string(3)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
 **/

class Countries_model extends CI_Model {    
	
    var $table = 'countries';

    public function __construct() {
		parent::__construct();
    }

    /**
     * @param id int  
     * @return Object (Info about determined a country)
     **/
    public function get_data($id)
    {
        $this->db->where('id',$id);
        return $this->db->get($this->table)->row();
    }

    /**
     * @return Array (List all countries in the system)
     **/
    public function get_all()
    {
        $this->db->where('deleted','0');
        $this->db->where('state','1');
        return $this->db->get($this->table)->result();
    }


}


