<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  auth_audit
 * 
 * ---------------Atrributes---------------
    session int
    action  string
    table   string
    object int
    time datetime
 **/

class Audit_model extends CI_Model {    
	
    private $table = 'auth_audit';
    var $column_order = array(null, 'auth_users.username','auth_audit.table','auth_audit.object','description','time'); 
    var $column_search = array('description');
    var $order = array('id','desc');
    
    public function __construct() {
		parent::__construct();
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('auth_audit.id,table,object,description,time,auth_users.username');
        $this->db->join('sessions','sessions.id = auth_audit.session');
        $this->db->join('auth_users','auth_users.id = sessions.user');
        $this->db->from($this->table);
        
       // echo var_dump(urldecode($_GET['log_session']));exit;
        //Exist any session to filter
        if(isset($_GET['log_session']) && !empty($_GET['log_session']))
        {
            $session = $this->security->xss_clean($this->input->get('log_session'));
            $this->db->where('session',$session);
        }

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by('id', 'desc');
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/

    /**
     *
     * Save object in the table
     * 
     * @param data array('session','action','table','object','description','time')
     * @return Boolean 
     **/
    public function save($data)
    {
        //Sessión activa del suaurio que realiza la operación
        $data['session'] = $this->session->session_id;
        return $this->db->insert($this->table,$data);
    }

}


