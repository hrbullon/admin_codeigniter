<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  coins
 * 
 * ---------------Atrributes---------------
    id int
    name  string 
    symbol string
    state tinyint ( 0 -> Inactivo, 1 -> Activo)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
 **/

class Coins_model extends CI_Model {    
	
    var $table = 'coins';

    public function __construct() {
		parent::__construct();
    }

    /**
     * @param id int  
     * @return Object (Info about determined a coin)
     **/
    public function get_data($id)
    {
        $this->db->where('id',$id);
        return $this->db->get($this->table)->row();
    }

    /**
     * @return Array (List all coins in the system)
     **/
    public function get_all()
    {
        $this->db->where('deleted','0');
        $this->db->where('state','1');
        return $this->db->get($this->table)->result();
    }


}


