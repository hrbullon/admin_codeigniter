<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  settings
 * 
 * ---------------Atrributes---------------
    id int
    social  string (Name)
    fiscal string (RIF)
    email string
    local_phone string
    local_mobile string
    country int (id of country ej -> 1 = Venezuela)
    city string (name of city, departament, state and other)
    coin int (id of coin ej -> 1 = Bolivar)
    tax int (id of tax ej -> 1 = IVA)
    format_date string (ej 01-01-2018)
    separator char (Character like "," or "." to separate decimals)
    decimals tinyint (Number of decimals in quantities)
    protocole tinyint (Id of protocole ej -> 1 = SMTP)
    outserver string (url, link or host ej: mail.smtp.com)
    outport char (Number of port)
    out_username string (Emaill addres of some user)
    out_password string (Passowrd for out_username)

 **/

class Settings_model extends CI_Model {

    var $table = 'settings';

    public function __construct() {
		parent::__construct();
    }

    /**
     * @return Object (Info about settings of system)
     **/
    public function get_data($company)
    {
        $this->db->where('company',$company);
        return $this->db->get($this->table)->row();
    }

    public function save($data, $id = null)
    {   
        //Start transaction
        $this->db->trans_begin();

        //Save new data in settings
        if(!$id){
            $id = ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;
        }else{
            $this->db->where(array('id'=>$id));
            $this->db->update($this->table,$data);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return true;
        }
    }

    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            
            //Save logo company
            case 1:

                $data['action'] = "Update logo company";
                $data['table']  = "settings";
                $data['object'] = $object;
                $data['description'] = "Actualzición de logo de la empresa";
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Save vars sytem    
            case 2:

                $data['action'] = "Update vars system";
                $data['table']  = "settings";
                $data['object'] = $object;
                $data['description'] = "Actualización de variables del sistema";
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Save email config    
            case 3:

                $data['action'] = "Update email config";
                $data['table']  = "settings";
                $data['object'] = $object;
                $data['description'] = "Actualización de configuraciones de servidor de correo";
                $data['time'] = date('Y-m-d H:i:s');

                break;
                
        }

        $data['model'] = $model;

        return  $data;

    }



}


