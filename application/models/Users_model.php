<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * CodeIgniter Model Class
 *
 *
 * @package     CodeIgniter
 * @category    Model
 * @author      Haderson Bullón
 *
 *  --------------Table--------------------
 *
 *  auth_users
 * 
 * ---------------Atrributes---------------
    id int
    profile  string (path to image profile)
    username string
    password string
    state tinyint (0 -> Inactivo, 1 -> Activo)
    deleted tinyint ( 0 -> Visible, 1 -> Eliminado)
 **/

class Users_model extends CI_Model {    
	
    //Name of table in database
    private $table = 'auth_users';
    private $column_order = array(null, 'u.username','u.state'); 
    private $column_search = array('u.username','u.state');    	
	private $order = array('u.id' => 'desc');

    public function __construct() {
		parent::__construct();
    }

    /**
     * @param id int  
     * @return Object (Info about determined a user)
     **/
    public function get_data($id)
    {
        $this->db->where('id',$id);
        $user = $this->db->get($this->table)->row();
        
        $data = array();

        array_push($data, array(
            'user' => $user,
            'roles' => $this->get_roles($id)
        ));

        return $data[0];
    }

    /**
     * @param user int  
     * @return Array (List of roles for one user)
     **/
    private function get_roles($user)
    {
        $this->db->where('user',$user);
        $this->db->where('deleted','0');
        return $this->db->get('auth_user_roles')->result();
    }

    /**
     * @param username string  
     * @return Object (Info about de username)
     **/
    public function get_user_by_username($username)
    {   

        $this->db->select('auth_users.*,users_companies.company');
        $this->db->from($this->table);
        $this->db->join('users_companies', 'users_companies.user = auth_users.id');
        return $this->db->where('auth_users.username',$username)->get()->row();
    }

    /**
     *
     * Update password in a determinated user
     * 
     * @param user int
     * @param password string  
     * @return Boolean
     **/
    public function update_password($user,$password)
    {
        $data['password'] = password_hash($password.SALT_HASH, PASSWORD_DEFAULT);
        $this->db->where(array('id'=>$user));
        return $this->db->update($this->table,$data);
    }

    /**
     *
     * Update image profile of user
     * 
     * @param user int
     * @param picture string  
     * @return Boolean
     **/
    public function update_picture($user,$picture)
    {
        $data['profile'] = $picture;
        $this->db->where(array('id'=>$user));
        return $this->db->update($this->table,$data);
    }

    /**
     *
     * Update image profile of user
     * 
     * @param user int
     * @param data array ('theme'=>'default',1/2)  
     * @return Boolean
     **/
    public function save_preferences($user,$data)
    {
        $this->db->where(array('id'=>$user));
        return $this->db->update($this->table,$data);
    }

    /**
     *
     * Save object in the table
     * 
     * @param data array('profile','username','password','state')
     * @param roles array()
     * @param id int  id of determinated user
     * @return Boolean 
     **/
    public function save($data,$roles,$id)
    {
        //Start transaction
        $this->db->trans_begin();
        
        if(!$id)
        {   
            //Insert user
            $data['deleted'] = 0;
            $id = ($this->db->insert($this->table,$data))? $this->db->insert_id() : null;

            $array = array();

            //Preparo roles a insertar
            foreach ($roles as $value) {
                array_push($array, array(
                    'user' => $id,
                    'role' => $value,
                    'deleted' => '0'
                ));
            }

            //Inserto los roles
            $this->db->insert_batch('auth_user_roles', $array);

        }else{

            //Guardo los datos del usuario
            $this->db->where(array('id'=>$id));
            $this->db->update($this->table,$data);

            //Verificar cual de los roles enviados no está
            $user_roles = $this->get_roles($id);

            //Eliminar roles
            foreach ($user_roles as $role) {
                $flag = false;
                foreach ($roles as $value) {
                    if($value == $role->role){
                        $flag = true;
                    }
                }

                //Si es falss es porque lo han eliminado
                if(!$flag){
                    //Seteo el campo deleted a "1" para que aparezca eliminado
                    $deleted['deleted'] = 1;
                    $this->db->where('user',$id);
                    $this->db->where('role',$role->role);
                    $this->db->update('auth_user_roles',$deleted);
                }
            }
            
            //Agregar nuevos roles
            foreach ($roles as $value) {
                $this->db->where('user',$id);
                $this->db->where('role',$value);
                $find = $this->db->get('auth_user_roles')->row();

                if($find){
                    //Si el rol existe pero está eliminado
                    if($find->deleted == 1){

                        $act['deleted'] = 0;
                        $this->db->where('user',$id);
                        $this->db->where('role',$value);
                        $this->db->update('auth_user_roles',$act);
                    }
                }else{
                    //Inserto el nuevo role
                    $new['user'] = $id;
                    $new['role'] = $value;
                    $new['deleted'] = 0;

                    $this->db->insert('auth_user_roles',$new);
                }
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return $id;
        }
    }  

    /**
     *
     * Assign some user to company
     * 
     * @param user int id of new user
     * @param company int id company of session 
     * @return Boolean 
     **/
    public function save_user_company($user,$company)
    {   
        $data['user'] = $user;
        $data['company'] = $company;

        return ($this->db->insert('users_companies',$data))? $this->db->insert_id() : null;
    }

    /**
     *
     * Logical delete one item selected
     * 
     * @param id int  id of determinated user
     * @return Boolean 
     **/
    public function delete($id)
    {   
        $data['deleted'] = 1;
        $this->db->where('id', $id);
        return $this->db->update($this->table,$data);
    }

    /***************************************************************/
    /*************Functions for build a datatable grid**************/
    /***************************************************************/
    private function _get_datatables_query()
    {
        $this->db->select('u.id,u.username,u.state');
        $this->db->from($this->table." u");
        $this->db->where("deleted","0");

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table." u");
        return $this->db->count_all_results();
    }

    /***************************************************************/
    /*******End functions for build datatable grid*****/
    /***************************************************************/
    
    /**
     *
     * Get info about some action in the table
     * 
     * @param option int (case to execute)
     * @param object int  (Identificator of object in the table)
     * @return Array ('action','table','object','description','time')
     **/
    public function get_data_audit($option, $object, $model)
    {
        $data = array();
        $model_decode = json_decode($model);

        switch ($option) {
            //Create user
            case 1:

                $data['action'] = "Create";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Creación del usuario: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Update user
            case 2:

                $data['action'] = "Update";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Actualización del usuario: ".$model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Delete user
            case 3:

                $data['action'] = "Delete";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Eliminación del usuario: ". $model_decode->user->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Login - incorrect user
            case 4:

                $data['action'] = "Login";
                $data['description'] = "Inicio de sesión fallido usuario incorrecto: ". $model_decode->username ;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Login - incorrect password
            case 5:

                $data['action'] = "Login";
                $data['description'] = "Inicio de sesión fallido contraseña incorrecta: ". $model_decode->username ;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Login - successful
            case 6:

                $data['action'] = "Login";
                $data['table']  = "sessions";
                $data['object'] = $object;
                $data['description'] = "Inicio de sesión satisfactorio: ". $model_decode->username ;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Update image profile - invalid
            case 7:

                $data['action'] = "Actualizar imagen de perfil";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Formato de archivo inválido: ". $model_decode->file ;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Update password 
            case 8:
                
                $data['action'] = "Update password";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = " Actualización de contraseña usuario: " . $model_decode->user->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Close session 
            case 9:
                
                $data['action'] = "Logout";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Cierre de sesión: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            //Receovery access failed 
            case 10:
                
                $data['action'] = "Mail recovery password can't be sent";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "No se pudo enviar el correo de restablecer contraseña: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;
            case 11:
                
                $data['action'] = "Mail recovery password sent";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Correo de restablecer contraseña enviado: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;   
            case 12:
                
                $data['action'] = "Try to recopvery password with incorrect username";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Intento de recuperar contraseña con correo icorrecto: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;    
            case 13:
                
                $data['action'] = "Change password from recovery form";
                $data['table']  = "users";
                $data['object'] = $object;
                $data['description'] = "Restablecimiento de contraseña para el usuario: " . $model_decode->username;
                $data['time'] = date('Y-m-d H:i:s');

                break;                         
        }

        $data['model'] = $model;

        return  $data;

    }
}


