<div class="content">
    <!-- begin page-header -->
    <h1 class="page-header">Configuraciones</h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title">Configuraciones Generales</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab_left">
                        <ul class="nav nav-tabs nav-red">
                            <li class="active">
                                <a href="#tab3_1" data-toggle="tab">
                                    <i class="icon-home"></i> Datos Empresa
                                </a>
                            </li>
                            <li >
                                <a href="#tab3_2" data-toggle="tab">
                                    <i class="icon-home"></i> Logo empresa
                                </a>
                            </li>
                            <li >
                                <a href="#tab3_3" data-toggle="tab">
                                    <i class="icon-user"></i> Variables de sistema
                                </a>
                            </li>
                            <li>
                                <a href="#tab3_4" data-toggle="tab">
                                    <i class="icon-cloud-download"></i> Correo
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab3_1">
                                <?php $this->load->view('settings/company');?>
                            </div>
                            <div class="tab-pane fade" id="tab3_2">
                                <?php $this->load->view('settings/logo');?>
                            </div>
                            <div class="tab-pane fade" id="tab3_3">
                                <?php $this->load->view('settings/general');?>
                            </div>
                            <div class="tab-pane fade" id="tab3_4">
                                <?php $this->load->view('settings/email');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $.when(
        $.getScript('assets/js/settings.min.js'),
        $.Deferred(function( deferred ){
            $(deferred.resolve);
        })
    ).done(function() {
        FormSettings.init();
    });
</script>
