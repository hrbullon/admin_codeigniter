<style type="text/css">
p{
    font-size: 10px;
    padding: 0px;
    margin: 5px;
}
</style>
<page backtop="30mm" backbottom="10mm" > 
<page_header>
    <?php $this->load->view('htmlpdf/header');?>
</page_header> 
<page_footer> 
<p align="right">página [[page_cu]]/[[page_nb]]</p>
</page_footer> 
<p>&nbsp; &nbsp; &nbsp;</p>
<div id="header">
    <table style="height: 150px; width: 622px;margin-left: 10px;">
        <tbody>
            <tr>
                <td style="width: 355px;border: solid;1px;red">
                    <p>DATOS DEL CLIENTE.-</p>
                    <p>Nombre o Raz&oacute;n social: CLIENTE DE PRUEBA</p>
                    <p>Domicilio:  POR ESTAS CALLES</p>
                    <p>RIF:J9801309821</p>
                    <p>Atencion Sr(a)(ta) : FULANITO</p>
                </td>
                <td style="width: 355px;border: solid;1px;red">
                    <p>CODICIONES GENERALES</p>
                    <p>Precios: Incluye I.G.V 18%</p>
                    <p>Plazo de Entrega: Inmediata</p>
                    <p>Forma de Pago: Contado</p>
                    <p>Garant&iacute;a: Especificado por cada producto</p>
                </td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp; &nbsp; &nbsp;</p>
    <p style="font-size: 12px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Estimados Se&ntilde;ores. Por medio de la presente, y de acuerdo a su solicitud, sometemos a su consideracion nuestra oferta <br>como sigue:</p>
    <p>&nbsp; &nbsp; &nbsp;</p>
</div>  
<table  cellspacing="0" style="width: 100%; border: solid 1px black; background: #F7F7F7; font-size: 8pt;margin-left: 10px;">
      <thead>
        <tr>
          <td style="text-align: center;border-bottom: solid 1px black;">IT</td>
          <td style="text-align: center;border-bottom: solid 1px black;">Codigo</td>
          <td style="text-align: center;border-bottom: solid 1px black;">Nombre o Descripcion</td>
          <td style="text-align: center;border-bottom: solid 1px black;">Unit</td>
          <td style="text-align: center;border-bottom: solid 1px black;">Precio &nbsp;Unit.</td>
          <td style="text-align: center;border-bottom: solid 1px black;">Precio Total</td>
        </tr>
      </thead>
      <tbody>
          <?php
              $total = 0;
               for($x = 1; $x <= 200; $x++): ?>
                  <tr>
                    <td style="width: 40px;text-align: center;"><?php echo $x;?></td>
                    <td style="width: 150px;text-align: center;"><?php echo "Pro Cod.".$x;?></td>
                    <td style="width: 280px;"><?php echo "Pro desc.".$x;?></td>
                    <td style="width: 70px;text-align: center;">10</td>
                    <td style="width: 70px;text-align: center;">10.00</td>
                    <td style="width: 90px;text-align: center;">100.00</td>
                  </tr>
          <?php 
                $x++;
               endfor;?> 
          <tr>
              <td></td><td></td><td></td><td></td><td></td><td></td>
              <td style="text-align: center;">Total 1222:</td>
              <td style="text-align: center;"> 1222</td>
          </tr>
      </tbody>
  </table>
  <p>&nbsp; &nbsp; &nbsp;</p>
  <p style="font-size: 12px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sin otro particular y en espera de su aceptacion a la presente, quedamos a su disposicion para cualquier consulta que ustedes estimen conveniente.</p>
  

  <table style="margin-top: 200px">
    <tr>
       <td style="width: 400px">
          <p>Cualquier Consulta :<br> 
             252-3333 / 252-3330 / 252-3304 / 252-3307 
          </p>
       </td> 
       <td>_________________________________________</td> 
    </tr>
    <tr>
       <td>
        <p>email : <br> hbullon@gruposerex.com <br>
        </p>
       </td> 
       <td style="text-align: center;"><p>Haderson Bullón <br>Ejecutivo(a) de Ventas</p></td> 
    </tr>
  </table> 

  <table style="margin-top: 100px">
    <tr>
       <td style="width: 400px">
         <p>Esta cotización tiene válida durante 15 días.</p>
       </td> 
    </tr>
     <tr>
       <td style="width: 400px">
          <p>Monto del flete: 10000.00</p>
       </td> 
    </tr>
  </table>  
</page> 

