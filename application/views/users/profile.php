<div class="content">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#default-tab-1" data-toggle="tab">Mis datos</a></li>
        <li class=""><a href="#default-tab-2" data-toggle="tab">Apariencia</a></li>
        <li class=""><a href="#default-tab-3" data-toggle="tab">Historial de operaciones</a></li>
        <li class=""><a href="#default-tab-4" data-toggle="tab">Ultimas sesiones</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <div class="profile-container">
                <!-- begin profile-section -->
                <div class="profile-section">
                    <!-- begin profile-left -->
                    <div class="profile-left">
                        <!-- begin profile-image -->
                        <div class="profile-image">
                            <img  id="image-profile" src="<?php echo $this->session->userdata('profile');?>" width="300px" height="175px">
                            <i class="fa fa-user hide"></i>
                        </div>
                        <!-- end profile-image -->
                        <div class="m-b-10">
                            <input type="file" name="img-profile" class="hidden" accept="image/png, .jpeg, .jpg, image/gif">
                            <button id="btn-change-image" class="btn btn-warning btn-block btn-sm">Cambiar foto</button>
                            <button id="btn-image-selected" class="hidden btn btn-primary btn-block btn-sm"><i class="fa fa-save"></i> Guardar imagen</button>
                        </div>
                    </div>
                    <!-- end profile-left -->
                    <!-- begin profile-right -->
                    <div class="profile-right">
                        <!-- begin profile-info -->
                        <div class="profile-info">                            
                            <form id="form-update-password" data-parsley-validate="">
                            <!-- begin table -->
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">Datos</td>
                                            <td></td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">Usuario</td>
                                            <td>
                                                <?php if(!is_null($this->session->userdata('username'))){ ?>
                                                    <?php echo $this->session->userdata('username');?>
                                                <?php } ?>
                                            </td>    
                                        </tr>
                                        <tr>
                                            <td class="field">Contraseña actual *</td>
                                            <td>
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                                                <input type="password" name="password_old" class="form-control" required>
                                            </td>    
                                        </tr>
                                        <tr>
                                            <td class="field">Contraseña nueva *</td>
                                            <td>
                                                <input type="password" id="password_new" name="password_new" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">Confirmar contraseña *</td>
                                            <td>
                                                <input type="password" name="password_confirm" class="form-control" required data-parsley-equalto="#password_new">
                                            </td>
                                        </tr>
                                </table>
                            </div>
                            <!-- end table -->
                            <button id="btn-update-password" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i> Guardar
                            </button>
                            </form>
                        </div>
                        <!-- end profile-info -->
                    </div>
                    <!-- end profile-right -->
                </div>
                <!-- end profile-section -->
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <div class='row'>
                <div class='col col-md-6 col-lg-6 col-sm-12 col-xs-12'>
                    <label> Seleccione un tema de su agrado</label><br>
                    <ul class="theme-list clearfix">
                        <li class="<?php echo ($this->session->theme == "default")? "active":"";?>">
                            <a href="javascript:;" class="bg-cyan" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default/Cyan">&nbsp;</a></li>
                        <li class="class="<?php echo ($this->session->theme == "blue")? "active":"";?>">
                            <a href="javascript:;" class="bg-blue" data-theme="blue" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a></li>
                        <li class="<?php echo ($this->session->theme == "purple")? "active":"";?>">
                            <a href="javascript:;" class="bg-purple" data-theme="purple" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a></li>
                        <li class="<?php echo ($this->session->theme == "orange")? "active":"";?>">
                            <a href="javascript:;" class="bg-orange" data-theme="orange" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a></li>
                        <li class="<?php echo ($this->session->theme == "default")? "red":"";?>">
                            <a href="javascript:;" class="bg-red" data-theme="red" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>
                        <li class="<?php echo ($this->session->theme == "default")? "black":"";?>">
                            <a href="javascript:;" class="bg-black" data-theme="black" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">&nbsp;</a></li>
                    </ul>
                </div>
            </div>
            <br>
            <div class='row'>
                <div class='col col-md-6 col-lg-6 col-sm-12 col-xs-12'>     
                    <div class="divider"></div>
                    <label>Apariencia de la cabecera</label>
                    <select name="header-styling" class="form-control input-sm">
                        <option value="navbar-default" <?php echo ($this->session->style_header == "navbar-default")? "selected":"";?>>Normal</option>
                        <option value="navbar-inverse" <?php echo ($this->session->style_header == "navbar-inverse")? "selected":"";?>>Inverso</option>
                    </select>
                </div>
            </div>
            <button onclick="HandlePreferences()" class="btn btn-primary pull-right">
                <i class="fa fa-save"></i> Guardar
            </button>    
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <div class="row">
                <div class="col-md-12">
                    <table id="history-table" class="table table-striped display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Descripción</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>    
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-4">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-danger btn-md" onclick="HandleCloseAllSessions()">
                        <i class="fa fa-close"></i> Cerrar sesiones activas
                    </button><br><br>
                    <table id="history-table" class="table table-striped display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Dirección IP</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $x = 1;
                                foreach ($sessions as $item) { ?>
                                <tr>
                                    <td><?php echo $x; ?></td>
                                    <td><?php echo $item->ip_address; ?></td>
                                    <td><?php echo $item->start_time; ?></td>
                                    <td><?php echo $item->end_time; ?></td>
                                    <td>
                                    <?php if($item->state == 0){
                                        echo "<span class='label label-warning'> Terminada </span>";
                                    }else{
                                        echo "<span class='label label-success'> Activo </span";
                                    }?>
                                    </td>
                                </tr>
                            <?php $x++;
                                } ?> 
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    </div>
</div>
<script>

    $.when(
        $.getScript('assets/js/profile.min.js'),
        $.Deferred(function( deferred ){
            $(deferred.resolve);
        })
    ).done(function() {
        ProfileUser.init();
    });
</script>   
