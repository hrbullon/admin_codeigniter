<!-- ================== BEGIN PAGE CSS STYLE ================== -->	
<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

<!-- ================== END PAGE CSS STYLE ================== -->
<form id="form-users" data-parsley-validate="">
    <input type="hidden" name="user" value="<?php echo (isset($model['user']->id))? $model['user']->id: "";?>"/>
    <div class="form-group">
      <label>Usuario/Correo</label>
      <input class="form-control" required="" name="username" value="<?php echo (isset($model['user']->username))? $model['user']->username: "";?>">
    </div>
    <div class="form-group">
      <label>Tipo de usuario</label>
      <select style="width: 100%" required="" id="roles" class="multiple-select2 form-control" name="roles[]" multiple="multiple">
          <?php foreach ($roles as $value) { ?>
            <?php 
              $selected = "";  
              foreach ($model['roles'] as $role) {
                  if($role->role ==  $value->id){
                    $selected = "selected";
                  }
              }
            ?>
            <option <?php echo $selected;?> value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
          <?php }?>
      </select>
    </div>
    <div class="form-group">
      <label>Activo</label><br>
      <input type="checkbox" name="state" data-render="switchery" data-theme="blue"
      <?php echo (isset($model['user']->state) && $model['user']->state == 1)? "checked" : "";?>/>
    </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
    <button type="submit" id="btn-send-form-user" class="btn btn-primary">
    	<i class="fa fa-save"></i> Guardar
    </button>
    <button type="reset" class="btn btn-default">
    	<i class="fa fa-reset"></i> Cancelar
    </button>
</form>
<script>

	$.when(
		$.getScript('assets/plugins/switchery/switchery.min.js'),
		$.getScript('assets/js/form-slider-switcher.min.js'),
		$.getScript('assets/plugins/select2/dist/js/select2.min.js'),
		$.getScript('assets/js/form-users.min.js'),

		$.Deferred(function( deferred ){
			$(deferred.resolve);
		})
	).done(function() {
		FormSliderSwitcher.init();
		FormUser.init();
	});
</script>	