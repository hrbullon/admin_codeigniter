<!-- ================== BEGIN PAGE CSS STYLE ================== -->	
<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
<!-- ================== END PAGE CSS STYLE ================== -->
<form id="form-taxes">
	  <div class="form-group">
      <input type="hidden" name="tax" value="<?php echo (isset($model->id))? $model->id : "";?>" />
      <label>Nombre *</label>
      <input class="form-control" data-validate="required" name="name" value="<?php echo (isset($model->name))? $model->name: "";?>">
    </div>
    <div class="form-group">
      <label>Valor *</label>
      <input class="form-control" data-validate="required" name="value" value="<?php echo (isset($model->value))? $model->value: "";?>">
    </div>
    <div class="form-group">
      <label>Estado</label><br>
      <input type="checkbox" data-render="switchery" name="state" data-theme="blue" 
      <?php echo (isset($model->state) && $model->state == 1)? "checked" : "";?>/>
    </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
    <button type="button" id="btn-send-form-taxes" class="btn btn-primary">
    	<i class="fa fa-save"></i> Guardar
    </button>
    <button type="reset" class="btn btn-default">
    	<i class="fa fa-reset"></i> Cancelar
    </button>
</form>
<script>

	$.when(
		$.getScript('assets/plugins/switchery/switchery.min.js'),
		$.getScript('assets/js/form-slider-switcher.min.js'),
    $.getScript('assets/js/form-taxes.min.js'),
		$.Deferred(function( deferred ){
			$(deferred.resolve);
		})
	).done(function() {
		FormSliderSwitcher.init();
    FormTaxes.init();
	});
</script>	