<div class="content">
	<!-- begin page-header -->
	<h1 class="page-header">Menúes </h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
	    <div class="panel-heading">
	        <div class="panel-heading-btn">
	            <?php if(check_permission("menues_create")){ ?>
				    <a type="button" href="<?php echo base_url('app#menues/create');?>" class="btn btn-xs btn-default" data-click="panel-new"> 
				    	<i class="fa fa-plus"></i>
				    </a> 
			    <?php } ?>
	            <a href="javascript:;" class="btn btn-xs btn-default" onclick="refreshTableMenu();" data-click="panel-reload">
	            	<i class="fa fa-refresh"></i>
	            </a>
	            <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
	            	<i class="fa fa-expand"></i>
	            </a>
	        </div>
	        <h4 class="panel-title">Listado de menúes</h4>
	    </div>
	    <div class="panel-body">
	        <div class="row">
	            <div class="col-md-6">
	            	
	            </div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	                <table id="menus-table"  class="table table-striped table-hover display" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>Item</th>
	                            <th>Nombre</th>
	                            <th >Estado</th>
	                            <th class="text-center">Opciones</th>
	                        </tr>
	                    </thead>
	                </table>
                </div> 
	        </div>        
	    </div>
	</div>
</div>

<script>
	App.setPageTitle("Administración - Menúes");

	$.when(
		$.getScript('assets/js/menus.min.js'),
		$.Deferred(function( deferred ){
			$(deferred.resolve);
		})
	).done(function() {
		TableManageMenu.init(<?php echo $option_column;?>);
	});
</script>	
 
       