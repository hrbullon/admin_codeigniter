<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

<div class="content">
    <!-- begin page-header -->
    <h1 class="page-header">Menúes</h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <?php if(check_permission("menues_index")){ ?>  
                      <a href="<?php echo base_url('app#menues');?>" data-click="panel-list" class="btn btn-xs btn-default" >
                          <i class="fa fa-list"></i> 
                      </a>
                <?php } ?>
                <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title"><?php  echo $title;?> menú</h4>
        </div>
        <div class="panel-body">
            <div class="row">
              <form id="form-menu" data-parsley-validate="">
                <div class="col col-lg-6">
                  <div class="form-group">
                    <input type="hidden" name="menu" value="<?php echo (isset($model->id))? $model->id : "";?>"/>
                    <label>Nombre *</label>
                    <input class="form-control" required="" name="name" value="<?php echo (isset($model->name))? $model->name: "";?>">
                  </div>
                  <div class="form-group">
                    <label>Menú padre</label>
                    <select class="form-control" name="parent">
                        <option value="">Seleccione un menu padre</option>
                        <?php 
                         if(count($parents) > 0) {
                            foreach ($parents as $value) { ?>
                            <?php $selected_parent = (isset($model->parent) && $model->parent == $value->id)? "selected" : "";?>
                            <option <?php echo $selected_parent;?> value="<?php echo $value->id;?>">
                              <?php echo $value->name;?>
                            </option>
                        <?php }
                         } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Ruta *</label>
                    <input class="form-control"  name="route" value="<?php echo (isset($model->route))? $model->route: "";?>">
                  </div>
                  
                  <div class="form-group">
                    <label>Activo</label><br>
                    <input type="checkbox" data-render="switchery" name="state" data-theme="blue" 
                    <?php echo (isset($model->state) && $model->state == 1)? "checked" : "";?>/>
                  </div>

                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <button type="submit" id="btn-send-form-menu" class="btn btn-primary">
                      <i class="fa fa-save"></i> Guardar
                  </button>
                  <button type="reset" class="btn btn-default">
                      <i class="fa fa-reset"></i> Cancelar
                  </button>
                  
                </div>

                
                <div class="col col-lg-6">
                    <div class="form-group">
                          <label>Posición *</label>
                          <input class="form-control"  name="position" value="<?php echo (isset($model->position))? $model->position: "";?>">
                        </div>
                        <div class="form-group">
                          <label>Permiso *</label>
                          <select style="width: 100%"  id="permission" class="form-control select2" name="permission" >
                              <option value="">Seleccione un permiso</option>
                              <?php 
                              if(count($privileges) > 0) {
                                foreach ($privileges as $value) { ?>
                                    <?php $selected_permisssion = ($value->id == $model->permission)? "selected" : "";?>
                                    <option <?php echo $selected_permisssion;?> value="<?php echo $value->id;?>">
                                      <?php echo $value->name;?>
                                    </option>
                              <?php }
                               } ?> 
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Icono</label>
                          <input  class="form-control" name="icon" value="<?php echo (isset($model->icon))? $model->icon: "";?>">
                        </div>
                </div>

                
              </form>  
            </div>
        </div>
    </div>
</div>        
<div class="modal fade" id="dialog_icons" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Listado de Iconos</h4>
            </div>
            <div class="modal-body">
                <table id="append-icons" class="table table-striped table-hover"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> 
<script>
  App.setPageTitle("Administración - Menus");

  $.when(
    $.getScript('assets/plugins/switchery/switchery.min.js'),
    $.getScript('assets/js/form-slider-switcher.min.js'),
    $.getScript('assets/plugins/select2/dist/js/select2.min.js'),
    $.getScript('assets/js/menus.min.js'),

    $.Deferred(function( deferred ){
      $(deferred.resolve);
    })
  ).done(function() {
    TableManageMenu.init();
    FormSliderSwitcher.init();
  });
</script> 