        
        
        
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('assets/plugins/jquery/jquery-1.9.1.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jquery/jquery-migrate-1.1.0.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url('assets/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/jquery-cookie/jquery.cookie.js');?>"></script>
	<!-- ================== END BASE JS ================== -->

    <!--DATATABLES-->
    <script src="<?php echo base_url('assets/plugins/DataTables/media/js/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/js/buttons.colVis.min.js');?>"></script>

	<script src="<?php echo base_url('assets/plugins/parsley/dist/parsley.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/parsley/dist/i18n/es.js');?>"></script>
	
	<script src="<?php echo base_url('assets/js/apps.js');?>"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlqyoO8Smdmxq7Nh9hf24_vYVkiWhlAoE"></script-->
    <div id="scripts"></div>
	<script>
		$(document).ready(function() {
            App.init();
		});
	</script>

    <!--Global modal--->
    <div class="modal fade" id="global-modal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!--Notification modal-->
    <div class="modal fade" id="modal-notification">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle"></i>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-bullhorn"></i> Notificación</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-dismiss="modal" class="btn btn-sm btn-success">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>