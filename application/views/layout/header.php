<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Admin</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="hbullon" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/animate.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/style-responsive.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/theme/'.$this->session->theme.'.css');?>" rel="stylesheet" id="theme" data-theme-selected="<?php echo $this->session->theme;?>"/>
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<link href="<?php echo base_url('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/DataTables/extensions/Scroller/css/scroller.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/DataTables/extensions/ColReorder/css/colReorder.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/DataTables/extensions/Select/css/select.bootstrap.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/dropzone/min/dropzone.min.css');?>" rel="stylesheet" />
	
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/gritter/css/jquery.gritter.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/parsley/src/parsley.css');?>" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script data-pace-options='{ "ajax": true }' src="<?php echo base_url('assets/plugins/pace/pace.min.js');?>"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body>
	<?php $settings = get_settings_company();?>
	<!-- begin #page-loader -->
	<div id="page-loader">
	    <div class="material-loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
            <div class="message">Cargando...</div>
        </div>
	</div>
	<!-- end #page-loader -->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed page-with-wide-sidebar">
		<!-- begin #header -->
		<div id="header" class="header navbar <?php echo $this->session->style_header;?> navbar-fixed-top">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle-left" data-click="sidebar-minify">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo base_url(); ?>" class="navbar-brand">
					    <!-- Admin -->
						<?php if(!empty($settings->logo)) { ?>
							<img class='media-object' src="<?php echo base_url().$settings->logo; ?>" width="150" height="25">
						<?php } else{
							echo $this->config->item("project_name");
						} ?> 	
					</a>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
				    <li>
				        
				    </li>
					<li class="dropdown">
                        <a href="#" class="icon notification waves-effect waves-light" data-toggle="dropdown">
                            <i class="material-icons">notification_important</i> <span class="label label-notification">5</span>
                        </a>
						<ul class="dropdown-menu media-list pull-right animated fadeInDown">
                            <li class="dropdown-header bg-indigo text-white">Notificaciones (5)</li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="assets/img/user-1.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">John Smith</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">25 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="assets/img/user-2.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Olivia</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">35 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="material-icons media-object bg-deep-purple">people</i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New User Registered</h6>
                                        <div class="text-muted f-s-11">1 hour ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="material-icons media-object bg-blue">email</i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New Email From John</h6>
                                        <div class="text-muted f-s-11">2 hours ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="material-icons media-object bg-teal">shopping_basket</i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">You sold an item!</h6>
                                        <div class="text-muted f-s-11">3 hours ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer text-center">
                                <a href="javascript:;">View more</a>
                            </li>
						</ul>
					</li>
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo base_url($this->session->userdata('profile'));?>" alt="" /> 
							<span class="hidden-xs">Hola, <?php echo $this->session->userdata('username');?></span>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="<?php echo base_url('app#users/profile'); ?>">Editar Perfil</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url('auth/logout'); ?>">Cerrar Sesión</a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
					    <a href="#" data-toggle="nav-profile">
                            <div class="image">
                                <img src="<?php echo base_url($this->session->userdata('profile'));?>" alt="" />
                            </div>
						</a>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Menú principal</li>
					<?php get_menu_admin();?>
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		