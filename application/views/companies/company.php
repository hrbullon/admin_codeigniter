<form id="form-company-settings" data-parsley-validate="">
    <div class="col-lg-4">
        <div class="form-group">
            <input type="hidden" name="tab" value="1">
            <label for="social">Razón Social * </label>
            <input type="text" name="social" class="form-control" required="" placeholder="Ingrese una razón social" value="<?php echo (isset($model->social))? $model->social: "";?>">
        </div>
        <div class="form-group">
            <label for="fiscal">RIF *</label>
            <input type="text" name="fiscal" class="form-control" required="" placeholder="Ingrese su identificación fiscal" value="<?php echo (isset($model->fiscal))? $model->fiscal: "";?>">
        </div>
        <div class="form-group">
            <label for="email">Correo Electrónico *</label>
            <input type="text" name="email" class="form-control" required="" placeholder="Ingrese su correo electrónico" value="<?php echo (isset($model->email))? $model->email: "";?>">
        </div>
    </div>
    <div class="col-lg-4">
        <input type="hidden" name="company" value="<?php echo (isset($model->id))? $model->id : "";?>"/>
        <div class="form-group">
            <label for="local_phone">Teléfono Local *</label>
            <input type="text" name="local_phone" class="form-control" required="" placeholder="Ingrese un número de telf. local" value="<?php echo (isset($model->local_phone))? $model->local_phone: "";?>">
        </div>
        <div class="form-group">
            <label for="local_mobile">Teléfono Móvil *</label>
            <input type="text" name="local_mobile" class="form-control" required="" placeholder="Ingrese un número de telf. móvil" value="<?php echo (isset($model->local_mobile))? $model->local_mobile: "";?>">
        </div>
        <div class="form-group">
            <label for="country">País</label>
            <select name="country" class="form-control">
                <option value="">Seleccione un país</option>
                <?php foreach ($countries as $country) { ?>
                    <option <?php echo (isset($model->country) && $model->country == $country->id)? "selected": "";?> value='<?php echo $country->id;?>'><?php echo $country->name;?></option>
                <?php } ?>
            </select>
        </div>
    </div>    
    <div class="col-lg-4">
        <div class="form-group">
            <label for="city">Ciudad</label>
            <input type="text" name="city" class="form-control" placeholder="Ingrese el nombre de su localidad" value="<?php echo (isset($model->city))? $model->city: "";?>">
        </div>
        <div class="form-group">
            <label for="address">Dirección</label>
            <input type="text" name="address" class="form-control" placeholder="Ingrese la dirección" value="<?php echo (isset($model->address))? $model->address: "";?>">
        </div>
        <!-- Enabled only for user as admin -->
        <?php if(!preg_match('/settings/', $_SERVER['REQUEST_URI'])){ ;?>
        <div class="form-group">
            <label>Activo</label><br>
            <input type="checkbox" name="state" data-render="switchery" data-theme="blue"
            <?php echo (isset($model->state) && $model->state == 1)? "checked" : "";?>/>
        </div>
        <?php } ;?>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"/>
    </div>
    <div class="col-lg-12">
        <button class="btn btn-default pull-right" type="reset">
            <i class="fa fa-refresh"></i> Cancelar
        </button>
        <button type="submit" id="btn-company-save" class="btn btn-primary pull-right">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</form>
