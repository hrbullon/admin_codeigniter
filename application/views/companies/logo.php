<div class="row">
   <div class="col-md-3">
        <!-- begin logo-image -->
        <div class="profile-image">
            <input type="hidden" name="setting" value="<?php echo (isset($settings->id))? $settings->id : "";?>"/>
            <?php $img = (isset($settings->logo) && !empty($settings->logo))? $settings->logo : "assets/img/Logo_default.png";?>
            <img  id="image-logo" src="<?php echo $img; ?>" width="300px" height="175px">
            <i class="fa fa-user hide"></i>
        </div>
        <!-- end logo-image -->
        <div class="m-b-10">
            <input type="file" name="img-logo" class="hidden" accept="image/png, .jpeg, .jpg, image/gif">
            <button id="btn-change-logo" class="btn btn-warning btn-block btn-sm">Cambiar logo</button>
            <button id="btn-logo-selected" class="hidden btn btn-primary btn-block btn-sm">
                <i class='fa fa-save'></i> Guardar imagen
            </button>
        </div>
    </div>    
</div>