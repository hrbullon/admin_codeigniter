<!-- ================== BEGIN PAGE CSS STYLE ================== -->	
<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

<div class="content">
    <!-- begin page-header -->
    <h1 class="page-header"> <?php echo $title_header;?> </h1>
    <!-- end page-header -->
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <?php if(check_permission("companies_index") && !preg_match('/settings/', $_SERVER['REQUEST_URI'])){ ?>  
                      <a href="<?php echo base_url('app#companies');?>" data-click="panel-list" class="btn btn-xs btn-default" >
                          <i class="fa fa-list"></i> 
                      </a>
                <?php } ?>
                <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
            <h4 class="panel-title"><?php  echo $title;?></h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab_left">
                        <ul class="nav nav-tabs nav-red">
                            <li class="active">
                                <a href="#tab3_1" data-toggle="tab">
                                    <i class="icon-home"></i> Datos Empresa
                                </a>
                            </li>
                            <li >
                                <a href="#tab3_2" data-toggle="tab">
                                    <i class="icon-home"></i> Logo empresa
                                </a>
                            </li>
                            <li >
                                <a href="#tab3_3" data-toggle="tab">
                                    <i class="icon-user"></i> Variables de sistema
                                </a>
                            </li>
                            <!-- Enabled only for user as admin -->
                            <?php if(!preg_match('/settings/', $_SERVER['REQUEST_URI'])){ ;?>
                            <li>
                                <a href="#tab3_4" data-toggle="tab">
                                    <i class="icon-cloud-download"></i> Correo
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab3_1">
                                <?php $this->load->view('companies/company');?>
                            </div>
                            <div class="tab-pane fade" id="tab3_2">
                                <?php $this->load->view('companies/logo');?>
                            </div>
                            <div class="tab-pane fade" id="tab3_3">
                                <?php $this->load->view('companies/general');?>
                            </div>
                            <!-- Enabled only for user as admin -->
                            <?php if(!preg_match('/settings/', $_SERVER['REQUEST_URI'])){ ;?>
                            <div class="tab-pane fade" id="tab3_4">
                                <?php $this->load->view('companies/email');?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $.when(

        $.getScript('assets/plugins/switchery/switchery.min.js'),
		$.getScript('assets/js/form-slider-switcher.min.js'),
        $.getScript('assets/js/companies_form.min.js'),

        $.Deferred(function( deferred ){
            $(deferred.resolve);
        })
    ).done(function() {
        FormCompany.init();
        FormSliderSwitcher.init();
    });
</script>
