<form id="form-email-settings" data-parsley-validate="">
    <div class="col-lg-6">
        <div class="form-group">
            <input type="hidden" name="tab" value="3">
            <label for="protocolo">Protocolo *</label>
            <select name="protocolo" required="" class="form-control">
                <option value="">Seleccione un protocolo</option>
                <option value="1" <?php echo (isset($settings->protocolo) && $settings->protocolo == 1)? "selected" : "";?> >SMTP</option>
                <option value="2" <?php echo (isset($settings->protocolo) && $settings->protocolo == 2)? "selected" : "";?>>IMAP</option>
            </select>
        </div>
        <div class="form-group">
            <label for="outserver">Servidor *</label>
            <input type="text" name="outserver" required="" class="form-control" placeholder="Ingrese la dirección de su servidor saliente" value="<?php echo (isset($settings->outserver))? $settings->outserver: "";?>">
        </div>
        <div class="form-group">
            <label for="outport">Puerto *</label>
            <input type="text" name="outport" required="" class="form-control" placeholder="Ingrese el número del puerto saliente" value="<?php echo (isset($settings->outport))? $settings->outport: "";?>">
        </div>
    </div>
    <input type="hidden" name="setting" value="<?php echo (isset($settings->id))? $settings->id : "";?>"/>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"/>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="username">Nombre *</label>
            <input type="text" name="out_name" required="" class="form-control" placeholder="Ingrese un nombre" value="<?php echo (isset($settings->out_name))? $settings->out_name: "";?>">
        </div>
        <div class="form-group">
            <label for="username">Usuario *</label>
            <input type="text" name="username" required="" class="form-control" placeholder="Ingrese usuario" value="<?php echo (isset($settings->out_username))? $settings->out_username: "";?>">
        </div>
         <div class="form-group">
            <label for="password">Contraseña *</label>
            <input type="password" name="password" required="" class="form-control" placeholder="**********" value="<?php echo (isset($settings->out_password))? $settings->out_password: "";?>">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <button class="btn btn-default pull-right" type="reset">
                <i class="fa fa-refresh"></i> Cancelar
            </button>
            <button type="submit" id="btn-email-save" class="btn btn-primary pull-right">
                <i class="fa fa-save"></i> Guardar
            </button>
        </div>
    </div>        
</form>    