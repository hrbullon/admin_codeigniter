<form id="form-general-settings">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="coin">Moneda * </label>
            <select name="coin" class="form-control" require>
                <option value="">Seleccione una moneda</option>
                <?php foreach ($coins as $coin) { ?>
                    <option <?php echo (isset($settings->coin) && $settings->coin == $coin->id)? "selected": "";?> value='<?php echo $coin->id;?>'><?php echo $coin->name;?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="tax">Impuesto * </label>
            <select name="tax" class="form-control" require>
                <option value="">Seleccione un impuesto</option>
                <?php foreach ($taxes as $tax) { ?>
                    <option <?php echo (isset($settings->tax) && $settings->tax == $tax->id)? "selected": "";?> value='<?php echo $tax->id;?>'><?php echo $tax->name;?></option>
                <?php } ?>
            </select>
        </div>
        <input type="hidden" name="tab" value="2">
        <input type="hidden" name="setting" value="<?php echo (isset($settings->id))? $settings->id : "";?>"/>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"/>
        <div class="form-group">
            <label for="retencion">Porcentaje de retención * </label>
            <input type="text" name="retention" class="form-control" require value="<?php echo (isset($settings->retention))? $settings->retention: "";?>" placeholder="Ingrese el % de retención">
        </div>
        <div class="form-group">
            <label for="percepcion">Porcentaje de percepción * </label>
            <input type="text" name="perception" class="form-control" require value="<?php echo (isset($settings->perception))? $settings->perception: "";?>" placeholder="Ingrese el % de percepción">
        </div>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"/>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="date_format">Zona Horaria * </label>
            <select name="time_zone" class="form-control" require>
                <option value="">Seleccione una zona horaria</option>
                <?php foreach ($zones as $zone) { ?>
                    <option <?php echo (isset($settings->time_zone) && $settings->time_zone == $zone->description)? "selected": "";?> value='<?php echo $zone->description;?>'><?php echo $zone->description;?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="date_format">Formato de fecha * </label>
            <select name="format_date" class="form-control" require>
                <option value="">Seleccione un formato</option>
                <?php foreach ($formats as $format) { ?>
                    <option <?php echo (isset($settings->format_date) && $settings->format_date == $format->format)? "selected": "";?> value='<?php echo $format->format;?>'><?php echo $format->description;?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="decimals">Número de decimales * </label>
            <input type="text" name="decimals" class="form-control" require value="<?php echo (isset($settings->decimals))? $settings->decimals: "";?>" placeholder="Ingrese el número de decimales a mostrar en los precios y montos">
        </div>
        <div class="form-group">
            <label for="separator">Separador de miles * </label>
            <input type="text" name="separator" class="form-control" require value="<?php echo (isset($settings->separator))? $settings->separator: "";?>" placeholder="Ingrese el caractér separador de miles ej: punto (.) ">
        </div>
    </div>
    <div class="col-lg-12">
        <button class="btn btn-default pull-right" type="reset">
            <i class="fa fa-refresh"></i> Cancelar
        </button>
        <button id="btn-vars-save" class="btn btn-primary pull-right">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</form>