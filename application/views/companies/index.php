<div class="content">
	<!-- begin page-header -->
	<h1 class="page-header">Empresas </h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
	    <div class="panel-heading">
	        <div class="panel-heading-btn">
	        	<?php if(check_permission("companies_create")){ ?>
				<a href="<?php echo base_url('app#companies/create');?>" class="btn btn-xs btn-default" data-click="panel-new">
	            	<i class="fa fa-plus"></i>
	            </a>
	            <?php } ?>
	        	<a href="javascript:;" class="btn btn-xs btn-default" onclick="refreshTableCompanies();" data-click="panel-reload">
	            	<i class="fa fa-refresh"></i>
	            </a>
	            <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
	            	<i class="fa fa-expand"></i>
	            </a>
	        </div>
	        <h4 class="panel-title">Listado de empresas</h4>
	    </div>
	    <div class="panel-body">
	        <div class="row">
	        	<div class="col-md-12">
	                <table id="companies-table"  class="table table-striped table-hover display" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>Item</th>
								<th>Razón social</th>
								<th>RIF</th>
                                <th>Tel. Local</th>
                                <th>Tel. Móvil</th>
                                <th>Estado</th>
	                            <th class="text-center">Opciones</th>
	                        </tr>
	                    </thead>
	                </table>
                </div> 
	        </div>        
	    </div>
	</div>
</div>

<script>
	App.setPageTitle("Administración - Empresas");

	$.when(
		$.getScript('assets/js/companies.min.js'),
		$.Deferred(function( deferred ){
			$(deferred.resolve);
		})
	).done(function() {
		TableManageCompanies.init(<?php echo $option_column;?>);
	});
</script>	
 
       