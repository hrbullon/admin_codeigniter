<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />	
<!-- begin #content -->
<div id="content" class="content content-full-width">
	<!-- begin breadcrumb -->
	<!-- <ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Inicio</a></li>
		<li><a href="javascript:;">Permisos</a></li>
		<li class="active">Administración</li>
	</ol> -->
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Permisos</h1>
	<div class="p-20">
	<!-- begin row -->
	<div class="row">
	    <div class="panel panel-inverse">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-default" data-click="panel-expand">
		            	<i class="fa fa-expand"></i>
		            </a>
		            <a href="javascript:;" class="btn btn-xs btn-default" >
		            	<i class="fa fa-info-circle"></i>
		            </a>
		        </div>
		        <h4 class="panel-title">Administración de permisos</h4>
		    </div>
		    <div class="panel-body">
			    <!-- begin col-2 -->
			    <div class="col-md-2">
			        <div class="hidden-sm hidden-xs">
		                <h5 class="m-t-20">Roles</h5>
		                <ul class="nav nav-pills nav-stacked nav-roles">
		                <?php foreach ($roles as $role) { ?>
							<li data-rel="<?php echo $role->name;?>" class="">
							 	<a href="">
							 		<?php echo ucfirst(strtolower($role->name));?>
							    </a>
							</li>	
		                <?php } ?>

		                </ul>
		            </div>
		        </div>
	    		<!-- end col-2 -->
	   			<!-- begin col-10 -->
	    		<div class="col-md-10">
			        <div class="email-content">
		                <table id="privileges-table" 
		                class="table table-email table-hover display" cellspacing="0" width="100%">
		                    <thead>
		                        <tr>
		                            <th>
		                               Módulo/privilegio
		                            </th>
		                            <th>
		                            <input disabled type="checkbox" id="check_all" onclick="HandleCheckAll()">
		                               Todo
		                            </th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    </tbody>
		                </table>
			        </div>
			    </div>
			</div>        
	    </div>
	    <!-- end col-10 -->
	</div>
	<!-- end row -->
	</div>
</div>
<!-- end #content -->
<script>
	App.setPageTitle("Administración - Privilegios");

	$.when(
		$.getScript('assets/plugins/gritter/js/jquery.gritter.js'),
		$.getScript('assets/js/privileges.min.js'),
		$.Deferred(function( deferred ){
			$(deferred.resolve);
		})
	).done(function() {
		TableManagePrivileges.init();
	});
</script>	