<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content">
    <div class="row">
    	<div class="col-md-12">
            <div class="error">
			    <div class="error-code m-b-10 text-yellow">404 <i class="material-icons">warning</i></div>
			    <div class="error-content bg-grey-700">
			        <div class="error-message">No se ha podido encontrar la página solicitada</div>
			        <div class="error-desc m-b-20">
			            La página o elemento solicitado no existe. <br />
			        </div>
			    </div>
			</div>	
        </div>    
    </div>        
</div>