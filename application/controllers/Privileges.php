<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Privileges extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        //Check menu permission.
        check_activated_menu('Permisos');
        
		$this->load->model('Privileges_model','privileges');
		$this->load->model('Roles_model','roles');
	}

    /**
     *
     * Show Index privileges
     * @return View 
     **/
	public function index()
	{	
		$data['roles'] = $this->roles->get_all();
        $data['permissions'] = $this->db->get('auth_permissions')->result();
        
        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('privileges/index',$data);
        }
	}

    /**
     *
     * Get object list about permissions
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->privileges->get_datatables();

        $data = array();

        $no = $_POST['start'];
        $flag = "";
        foreach ($list as $item) {

            $no++;
            $row = array();

            $split = explode("_",$item->name);
            $description = explode(" ",$item->description);

            //Cada vez que cambie agregar fila extra
            if(count($description) == 1){

            	$row[] = "<b>".$item->description."</b>";
                $row[] = '<input disabled type="checkbox" data-rel="asign"  class="'.$item->name.'" onclick="HandleAsignRevoke(this,\''.$item->name.'\')" >';

            	$data[] = $row;

            	$flag = $item->description;
            }else{
            	$row[] = $item->description;
	            $row[] = '<input disabled type="checkbox" data-rel="asign"  class="'.$item->name.'" onclick="HandleAsignRevoke(this,\''.$item->name.'\')" >';
	            $data[] = $row;
            }

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->privileges->count_all(),
                "recordsFiltered" => $this->privileges->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }

    /**
     *
     * Asign or Revoke some permission 
     * @return JSON
     **/
    public function asign_revoke()
    {
    	if ($this->input->is_ajax_request()) {

    		$role = $this->input->post('role');
    		$action = $this->input->post('action');
    		$permissions = $this->input->post('permissions');

    		if ($this->privileges->asign_revoke($role,$action,$permissions)) {
    			$success = true;


                //Get info permission
                $permission = $this->privileges->get_data($permissions[0]);

                $model["role"] = $role;
                $model["permission"] = $permission->description;

                //Revoke permission
                if($action == "0"){

                    $model["action"] = "Remover permiso ". $permission->description;
                    $model_encode = json_encode($model);
                    $audit = $this->privileges->get_data_audit(2,"",$model_encode);

                //Assign permission
                }elseif ($action == "1") {

                    $model["action"] = "Asignar permiso ". $permission->description;
                    $model_encode = json_encode($model);
                    $audit = $this->privileges->get_data_audit(1,"",$model_encode);
                }

                $this->audit->save($audit);

    		}else {
    			$success = false;
    		}
            
    		$output = array('success'=>$success);
    		$this->output->set_content_type("application/json")->set_output(json_encode($output));
    	}else{
    		show_404();
    	}

    }

    /**
     *
     * Get all permissions about some role
     * @return JSON
     **/
    public function get_all()
    {
       if ($this->input->is_ajax_request()) {

    		$role = $this->input->post('role');

    		$result = $this->privileges->get_all_privileges($role);
    		$result = array('result'=> $result);
    		$this->output->set_content_type("application/json")->set_output(json_encode($result));
    	}else{
    		show_404();
    	}
    }

}

