<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
 **/

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        //Check menu permission.
        check_activated_menu('Usuarios');

        //Load models for this controller
        $this->load->model('Users_model','users');
        $this->load->model('Roles_model','roles');
	}
    
    /**
     *
     * Show Index roles
     * @return View 
     **/
	public function index()
	{
        //Hide column options in datatble
        if(check_permission("users_update") || check_permission("users_delete")){
            $data['option_column'] = 1;
        }else{
            $data['option_column'] = 0;
        }

        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('users/index',$data);
        }
	}

    /**
     *
     * Get object list about roles
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->users->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->username;
            if($item->state == 1){
                $state = "<span class='label label-success'> Activo </span>";   
            }else{
                $state = "<span class='label label-warning'> Inactivo </span>";
            } 
            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->users->count_all(),
                "recordsFiltered" => $this->users->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }

    /**
     *
     * Get object list about audit
     * @return JSON 
     **/
    public function history()
    {
        $list = $this->audit->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->description;
            $row[] = date("d-m-Y H:i:s", strtotime($item->time));

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->audit->count_all(),
                "recordsFiltered" => $this->audit->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }    

    /**
     *
     * Show View Profile
     * @return View 
     **/
    public function profile()
    {
        $data['sessions'] = $this->sessions_model->get_lastest_sessions();
    	$this->load->view('users/profile', $data);
    }

    /**
     *
     * Show Create screen
     * @return View 
     **/
    public function create()
    {   
        $data['roles'] = $this->roles->get_all();
    	$this->load->view('users/form',$data);
    }

    /**
     *
     * Show Update screen
     * @return View 
     **/
    public function update($id)
    {	
    	if($this->input->is_ajax_request())
    	{   
    		$user = $this->users->get_data($this->security->xss_clean($id));

    		if($user)
    		{
                $data['roles'] = $this->roles->get_all();
    			$data['model'] = $user;

    			$this->load->view('users/form', $data);
    		}else{
    			show_404();
    		}
    	}else{
    		show_404();
    	}
    	
    }

    /**
     *
     * Save data from Role form
     * @param username string
     * @param user string (Code)
     * @param roles array Ej: (1,2,3,4,5,6,7,8)
     * @param state int  (0 -> Inactivo, 1 -> Activo)
     * @return JSON
     **/
    public function save()
    {
        if($this->input->is_ajax_request())
        {
            $this->form_validation->set_rules('username', 'Usuario/Correo', 'required');
            $this->form_validation->set_rules('roles[]', 'Tipo de usuario', 'required');

            if ($this->form_validation->run() == TRUE) {

                $state = $this->security->xss_clean($this->input->post('state'));
                $codigo = $this->security->xss_clean($this->input->post('user'));

                $model['username'] = $this->security->xss_clean($this->input->post('username'));
                $model['state'] = (!is_null($state))? 1 : 0;
                
                //If a new user
                //Generate a password default
                if(!$codigo){

                    $hash_encode = base64_encode(date("dmyhis"));
                    $pass_hash = password_hash($hash_encode.SALT_HASH, PASSWORD_DEFAULT);
                    $model['password'] = $pass_hash;

                }

                $roles = $this->input->post('roles');

                //Save user with roles
                $user = $this->users->save($model, $roles ,$codigo);

                if($user)
                {
                    $option = (!$codigo)? 1 : 2;
                    $model['roles'] = $roles;
                    $model_json = json_encode($model);
                    //Get array otions to save audit
                    $audit = $this->users->get_data_audit($option, $user, $model_json);
                    //Save audit
                    $this->audit->save($audit);

                    //If a new user then send email with credentials
                    if($option == 1){
                        
                        $data['username'] = $model['username'];
                        $data['password'] = $hash_encode;
                        
                        //Assign company of session to new user
                        $this->users->save_user_company($user, $this->session->company);

                        $msg = $this->load->view("emails/new_access",$data, TRUE);
                        send_email($model["username"],"Nuevo usuario creado", $msg);
                    }

                    $success = true;
                    $code = 200;
                    $errors = array();

                }else{
                    $success = false;
                    $code = 500;
                    $errors = array('Error interno en el servidor');
                }
                
            } else {

                $success = false;
                $code = 200;

                $errors = array(
                    'username' => form_error('username'),
                    'roles[]' => form_error('roles[]')
                );
            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));

        }else{
            show_404();
        }
    }

    /**
     * Delete some row in the table
     * @param codigo string 
     * @return JSON
     **/
    public function delete($codigo)
    {
        if($this->input->is_ajax_request())
        {
            $codigo = $this->security->xss_clean($codigo); 
            $user = $this->users->get_data($codigo);

            if($user)
            {   
                $this->users->delete($user['user']->id);
                $code = 200;
                $success = true;

                $model_json = json_encode($user);
                //Get array otions to save audit
                $audit = $this->users->get_data_audit(3, $user['user']->id, $model_json);
                //Save audit
                $this->audit->save($audit);
            }else{
                $code = 200;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));


        }else{
            show_404();
        }
    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    private function buttons($id)
    {

        if(check_permission("users_update") || check_permission("users_delete")){
            
            $option  = '<div class="btn-group m-r-5 m-b-5">
                    <a href="javascript:;" data-toggle="dropdown" class="btn btn-xs btn-warning dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-bars"></i> Acciones
                            <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">';
                        if(check_permission("users_update")){
                            $option .= '<li >
                                          <a onclick="TableManageUsers.editUser(\''.$id.'\')" href="javascript:;" >
                                            <i class="fa fa-edit"> </i>  Editar
                                          </a>
                                        </li>';
                        }    

                        if(check_permission("users_delete")){
                            $option .= '<li >
                              <a onclick="TableManageUsers.deleteUser(\''.$id.'\')" href="javascript:;" >
                                <i class="fa fa-trash"> </i>  Eliminar
                              </a>
                            </li>';
                        }     
                    
                    $option .= '
                    </ul>
                </div>';
            return $option;
        }
        
    }

    /**
     *
     * Save data from Role form
     * @param foto FILE
     * @return JSON
     **/
    public function upload_profile_image()
    {
        if($this->input->is_ajax_request())
        {   
            //Directory
            $ruta = "uploads/users-profile/";
            //Get extension of image
            $info = new SplFileInfo($_FILES['foto']['name']);
            $ext = $info->getExtension();
    
            //Check image type
            if (exif_imagetype($_FILES['foto']['tmp_name']) !== IMAGETYPE_GIF &&
                exif_imagetype($_FILES['foto']['tmp_name']) !== IMAGETYPE_JPEG &&
                exif_imagetype($_FILES['foto']['tmp_name']) !== IMAGETYPE_PNG &&
                exif_imagetype($_FILES['foto']['tmp_name']) !== IMG_JPG )
            { 

                $code = 200;
                $success = false;
                $error = "El formato del archivo que intenta subir es inválido";

            }else{

                //Get data user
                $user = $this->users->get_data($this->session->user_id); 

                //Should save in audit if user try to upload some file not allowed    
                $model['file'] = $_FILES['foto']['name'];
                $model['extension'] = $ext;

                $fichero = $ruta.md5(date("DmYh-his")).".".$ext;

                if (move_uploaded_file($_FILES['foto']['tmp_name'], $fichero)) {
                    $this->users->update_picture($this->session->user_id,$fichero);

                    $code = 200;
                    $success = true;
                    $error = "";

                    //Remove file old picture profile
                    if(!is_null($user['user']->profile) && !empty($user['user']->profile)){
                        unlink($user['user']->profile);
                    }

                    //Set new picture at session
                    $this->session->profile = $fichero;

                    $session_id = $this->session->session_id;
                    //Encode in json format the request
                    $model = json_encode($model);
                    //Save audit
                    $audit = $this->users->get_data_audit(7,$session_id,$model);
                    $this->audit->save($audit);
                    
                }else{
                    $code = 200;
                    $success = false;
                    $error = "Ocurrió un problema al subir la imagen, intente más tarde!";
    
                } 
                
            }

            $output = array("success"=>$success,"error"=>$error);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));
        }
        
    }

    /**
     *
     * Save data from Role form
     * @param password_new string
     * @param password_old string
     * @param password_confirm string
     * @return JSON
     **/
    public function update_password()
    {
        if($this->input->is_ajax_request())
        {   
            //Validate rules
            $this->form_validation->set_rules('password_old', 'Contraseña actual', 'required|callback_check_old_password');
            $this->form_validation->set_rules('password_new', 'Nueva contraseña', 'required');
            $this->form_validation->set_rules('password_confirm', 'Confirmar contraseña', 'required|matches[password_new]');

            //If all ok
            if ($this->form_validation->run() == TRUE)
            {
                $user = $this->session->userdata('user_id');
                $password = $this->input->post('password_confirm');

                if($this->users->update_password($user,$password))
                {

                    $success = true;
                    $code = 200;
                    $errors = array();

                    $model = $this->users->get_data($this->session->user_id);
                    //Encode in json format the request
                    $model_encode = json_encode($model);
                    //Save audit
                    $audit = $this->users->get_data_audit(8,$this->session->user_id,$model_encode);
                    $this->audit->save($audit);

                }else{

                    $success = false;
                    $code = 500;
                    $errors = array('Error interno en el servidor');

                }

            }else{

                $success = false;
                $code = 200;

                $errors = array(
                    'password_old' => form_error('password_old'),
                    'password_new' => form_error('password_new'),
                    'password_confirm' => form_error('password_confirm')
                );
            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));

        }
    }

    /**
     *
     * Save data from Role form
     * @param foto FILE
     * @return JSON
     **/
    public function preferences()
    {
        if($this->input->is_ajax_request())
        {   
            $theme = $this->input->post('theme');
            $style = $this->input->post('style');

            $theme = $this->security->xss_clean($theme);
            $style = $this->security->xss_clean($style);
            
            $data['theme'] = $theme;
            $data['style_header'] = $style;

            if($this->users->save_preferences($this->session->user_id,$data))
            {
                $this->session->theme = $theme;
                $this->session->style_header = $style;

                $success = true;
                $code = 200;
                $errors = array();

            }else{

                $success = false;
                $code = 500;
                $errors = array('Error interno en el servidor');

            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));
        }
        
    }

    /**
     *
     * Check usernema and password of user loged
     * @param username string
     * @param password_old string
     * @return Boolean
     **/
    public function check_old_password()
    {
        //Get username from user logged
        $username = $this->session->userdata('username');
        $password = $this->security->xss_clean($this->input->post('password_old'));
                
        $user = $this->users->get_user_by_username($username);

        if ($user) {
            //Veriffy if password is correct!
            if(password_verify($password.SALT_HASH,$user->password))
            {
                return true;
            }else{
                $this->form_validation->set_message('check_old_password', 'Contraseña actual incorrecta');
                return false;
            }
        }
    }


}


