<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Taxes extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        //Check menu permission.
        check_activated_menu('Impuestos');
		$this->load->model('Taxes_model','taxes');
	}

    /**
     *
     * Show Index taxes
     * @return View 
     **/
	public function index()
	{
        //Le dice al datatable que oculte la columna de opciones
        if(check_permission("taxes_update") || check_permission("taxes_delete")){
            $data['option_column'] = 1;
        }else{
            $data['option_column'] = 0;
        }

        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('taxes/index',$data);
        }
	}

    /**
     *
     * Get object list about taxes
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->taxes->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->name;
            $row[] = $item->value." % ";

            if($item->state == 1){
                $state = "<span class='label label-success'> Activo </span>";   
            }else{
                $state = "<span class='label label-warning'> Inactivo </span>";
            } 
            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->taxes->count_all(),
                "recordsFiltered" => $this->taxes->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }

    /**
     *
     * Show Create screen
     * @return View 
     **/
    public function create()
    {
    	$this->load->view('taxes/form');
    }

    /**
     *
     * Show Update screen
     * @return View 
     **/
    public function update($id)
    {	
    	if($this->input->is_ajax_request())
    	{
    		$tax = $this->taxes->get_data($this->security->xss_clean($id));

    		if($tax)
    		{
    			$data['model'] = $tax;
    			$this->load->view('taxes/form', $data);
    		}else{
    			show_404();
    		}
    	}else{
    		show_404();
    	}
    	
    }

    /**
     *
     * Save data from Role form
     * @param name string
     * @param value string
     * @param state int  (0 -> Inactivo, 1 -> Activo)
     * @return JSON
     **/
    public function save()
    {
        if($this->input->is_ajax_request())
        {
            $this->form_validation->set_rules('name', 'Nombre', 'required');
            $this->form_validation->set_rules('value', 'Valor', 'required');

            if ($this->form_validation->run() == TRUE) {

                $codigo = $this->security->xss_clean($this->input->post('tax'));

                $model['name'] = $this->security->xss_clean($this->input->post('name'));
                $model['value'] = $this->security->xss_clean($this->input->post('value'));
                $model['state'] = $this->security->xss_clean($this->input->post('state'));

                if($this->taxes->save($model, $codigo))
                {
                    $success = true;
                    $code = 201;
                    $errors = array();

                    //Save audit
                    //Encode in json format the request
                    $model_encode = json_encode($model);
                    
                    //If update or create
                    if(!$codigo){
                        $audit = $this->taxes->get_data_audit(1,"",$model_encode);
                    }else{
                        $audit = $this->taxes->get_data_audit(2,$codigo,$model_encode);
                    }
                    
                    $this->audit->save($audit);

                }else{
                    $success = false;
                    $code = 500;
                    $errors = array('Error interno en el servidor');
                }
                
            } else {
                $success = false;
                $code = 200;
                $errors = array(
                    'name' => form_error('name'),
                    'value' => form_error('value')
                );
            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));

        }else{
            show_404();
        }
    }

    /**
     * Delete some row in the table
     * @param codigo string 
     * @return JSON
     **/
    public function delete($codigo)
    {
        if($this->input->is_ajax_request())
        {
            $codigo = $this->security->xss_clean($codigo); 
            $tax = $this->taxes->get_data($codigo);

            if($tax)
            {   
                $this->taxes->delete($tax->id);
                $code = 200;
                $success = true;

                $model_encode = json_encode($tax);
                    
                $audit = $this->taxes->get_data_audit(3,$codigo,$model_encode);
                $this->audit->save($audit);

            }else{
                $code = 200;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));


        }else{
            show_404();
        }
    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    private function buttons($id)
    {   
        if(check_permission("taxes_update") || check_permission("taxes_delete")){
            
            $option  = '<div class="btn-group m-r-5 m-b-5">
                    <a href="javascript:;" data-toggle="dropdown" class="btn btn-xs btn-warning dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-bars"></i> Acciones
                            <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">';
                        if(check_permission("taxes_update")){
                            $option .= '<li >
                                          <a onclick="TableManageTaxes.editTax(\''.$id.'\')" href="javascript:;" >
                                            <i class="fa fa-edit"> </i>  Editar
                                          </a>
                                        </li>';
                        }

                        if(check_permission("taxes_delete")){
                            $option .= '<li>
                                  <a onclick="TableManageTaxes.deleteTax(\''.$id.'\')" href="javascript:;" >
                                    <i class="fa fa-trash"> </i>  Eliminar
                                  </a>
                                </li>';
                        }     
                    
                    $option .= '
                    </ul>
                </div>';
            return $option;
        }
    }


}


