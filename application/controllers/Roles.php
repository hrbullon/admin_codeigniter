<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Roles extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        //Check menu permission.
        check_activated_menu('Roles');
		$this->load->model('Roles_model','roles');
	}

    /**
     *
     * Show Index roles
     * @return View 
     **/
	public function index()
	{
        //Le dice al datatable que oculte la columna de opciones
        if(check_permission("roles_update") || check_permission("roles_delete")){
            $data['option_column'] = 1;
        }else{
            $data['option_column'] = 0;
        }

        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('roles/index',$data);
        }
	}

    /**
     *
     * Get object list about roles
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->roles->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->name;

            if($item->state == 1){
                $state = "<span class='label label-success'> Activo </span>";   
            }else{
                $state = "<span class='label label-warning'> Inactivo </span>";
            } 
            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->roles->count_all(),
                "recordsFiltered" => $this->roles->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }

    /**
     *
     * Show Create screen
     * @return View 
     **/
    public function create()
    {
    	$this->load->view('roles/form');
    }

    /**
     *
     * Show Update screen
     * @return View 
     **/
    public function update($id)
    {	
    	if($this->input->is_ajax_request())
    	{
    		$role = $this->roles->get_data($this->security->xss_clean($id));

    		if($role)
    		{
    			$data['model'] = $role;
    			$this->load->view('roles/form', $data);
    		}else{
    			show_404();
    		}
    	}else{
    		show_404();
    	}
    	
    }

    /**
     *
     * Save data from Role form
     * @param name string
     * @param state int  (0 -> Inactivo, 1 -> Activo)
     * @return JSON
     **/
    public function save()
    {
        if($this->input->is_ajax_request())
        {
            $this->form_validation->set_rules('name', 'Nombre', 'required');

            if ($this->form_validation->run() == TRUE) {

                $codigo = $this->security->xss_clean($this->input->post('role'));

                $model['name'] = $this->security->xss_clean($this->input->post('name'));
                $model['state'] = $this->security->xss_clean($this->input->post('state'));

                if($this->roles->save($model, $codigo))
                {
                    $success = true;
                    $code = 201;
                    $errors = array();

                    //Save audit
                    //Encode in json format the request
                    $model_encode = json_encode($model);
                    
                    //If update or create
                    if(!$codigo){
                        $audit = $this->roles->get_data_audit(11,"",$model_encode);
                    }else{
                        $audit = $this->roles->get_data_audit(2,$codigo,$model_encode);
                    }
                    
                    $this->audit->save($audit);

                }else{
                    $success = false;
                    $code = 500;
                    $errors = array('Error interno en el servidor');
                }
                
            } else {
                $success = false;
                $code = 200;
                $errors = array(
                    'name' => form_error('name')
                );
            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));

        }else{
            show_404();
        }
    }

    /**
     * Delete some row in the table
     * @param codigo string 
     * @return JSON
     **/
    public function delete($codigo)
    {
        if($this->input->is_ajax_request())
        {
            $codigo = $this->security->xss_clean($codigo); 
            $role = $this->roles->get_data($codigo);

            if($role)
            {   
                $this->roles->delete($role->id);
                $code = 200;
                $success = true;

                $model_encode = json_encode($role);
                    
                $audit = $this->roles->get_data_audit(3,$codigo,$model_encode);
                $this->audit->save($audit);

            }else{
                $code = 200;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));


        }else{
            show_404();
        }
    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    private function buttons($id)
    {   
        if(check_permission("roles_update") || check_permission("roles_delete")){
            
            $option  = '<div class="btn-group m-r-5 m-b-5">
                    <a href="javascript:;" data-toggle="dropdown" class="btn btn-xs btn-warning dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-bars"></i> Acciones
                            <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">';
                        if(check_permission("roles_update")){
                            $option .= '<li >
                                          <a onclick="TableManageRoles.editRole(\''.$id.'\')" href="javascript:;" >
                                            <i class="fa fa-edit"> </i>  Editar
                                          </a>
                                        </li>';
                        }

                        if(check_permission("roles_delete")){
                            $option .= '<li>
                                  <a onclick="TableManageRoles.deleteRole(\''.$id.'\')" href="javascript:;" >
                                    <i class="fa fa-trash"> </i>  Eliminar
                                  </a>
                                </li>';
                        }     
                    
                    $option .= '
                    </ul>
                </div>';
            return $option;
        }
    }


}


