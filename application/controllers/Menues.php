<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Menues extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        //Check menu permission
        check_activated_menu('Menues');

		$this->load->model('Menus_model','menus');
        $this->load->model('Privileges_model','privileges');
	}

    /**
     *
     * Show Index menu
     * @return View 
     **/
	public function index()
	{
        //Le dice al datatable que oculte la columna de opciones
        if(check_permission("menues_update") || check_permission("menues_delete")){
            $data['option_column'] = 1;
        }else{
            $data['option_column'] = 0;
        }

        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('menues/index',$data);
        }
	}

    /**
     *
     * Get object list about menues
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->menus->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->name;
            if($item->state == 1){
                $state = "<span class='label label-success'> Activo </span>";   
            }else{
                $state = "<span class='label label-warning'> Inactivo </span>";
            } 
            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->menus->count_all(),
                "recordsFiltered" => $this->menus->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }
    
    /**
     *
     * Show Create screen
     * @return View 
     **/
    public function create()
    {   
        $data['title'] = "Crear";
        $data['parents'] = $this->menus->get_parent_menus();
        $data['privileges'] = $this->privileges->get_all();
        
    	$this->load->view('menues/form',$data);
    }

    /**
     *
     * Show Update screen
     * @return View 
     **/
    public function update($id)
    {	
    	if($this->input->is_ajax_request())
    	{
            $codigo = $this->security->xss_clean($id); 
    		$menu = $this->menus->get_data($codigo);

    		if($menu)
    		{ 
                $data['model'] = $menu;
                $data['parents'] = $this->menus->get_parent_menus();
                $data['privileges'] = $this->privileges->get_all();

                $data['title'] = "Editar";    

    			$this->load->view('menues/form', $data);
    		}else{
    			show_404();
    		}
    	}else{
    		show_404();
    	}
    	
    }

    /**
     *
     * Save data from menu form
     * @param name string
     * @param permission int
     * @param postition int 
     * @param route string
     * @param state int  
     * @return JSON
     **/
    public function save()
    {
        if($this->input->is_ajax_request())
        {
            $this->form_validation->set_rules('name', 'Nombre', 'required');

            $parent = $this->input->post('parent');

            if(!is_null($parent) && !empty($parent)){
                $this->form_validation->set_rules('route', 'Ruta', 'required');
                $this->form_validation->set_rules('permission', 'Permiso', 'required|numeric');
            }else{
                $this->form_validation->set_rules('position', 'Posición', 'required|numeric');
            }

            if ($this->form_validation->run() == TRUE) {

                $state = $this->input->post('state');
                $codigo = $this->security->xss_clean($this->input->post('menu'));

                $model['name'] = $this->security->xss_clean($this->input->post('name'));
                $model['route'] = $this->security->xss_clean($this->input->post('route'));
                $model['position'] = $this->security->xss_clean($this->input->post('position'));

                if( !is_null($this->input->post('parent')) && !empty($this->input->post('parent')) )
                {
                    $model['parent'] = $this->security->xss_clean($this->input->post('parent'));
                }
                
                $model['permission'] = $this->security->xss_clean($this->input->post('permission'));
                $model['icon'] = $this->security->xss_clean($this->input->post('icon'));
                $model['state'] = (!is_null($state))? 1 : 0;
                $model['deleted'] = 0;


                if($this->menus->save($model, $codigo))
                {
                    $success = true;
                    $code = 200;
                    $errors = array();

                    //Save audit
                    //Encode in json format the request
                    $model_encode = json_encode($model);
                    
                    //If update or create
                    if(!$codigo){
                        $audit = $this->menus->get_data_audit(1,"",$model_encode);
                    }else{
                        $audit = $this->menus->get_data_audit(2,$codigo,$model_encode);
                    }
                    
                    $this->audit->save($audit);

                }else{
                    $success = false;
                    $code = 500;
                    $errors = array('Error interno en el servidor');
                }
                
            } else {

                $success = false;
                $code = 200;

                if(!is_null($parent) && !empty($parent)){
                    $errors = array(
                        'name' => form_error('name'),
                        'route' => form_error('route'),
                        'permission' => form_error('permission'),
                        'state' => form_error('state')
                    );
                }else{
                    $errors = array(
                        'name' => form_error('name'),
                        'position' => form_error('position'),
                        'state' => form_error('state')
                    );
                }
            }

            $output = array("success"=>$success,"errors"=>$errors);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));

        }else{
            show_404();
        }
    }

    /**
     * Delete some row in the table
     * @param codigo string 
     * @return JSON
     **/
    public function delete($codigo)
    {
        if($this->input->is_ajax_request())
        {
            $codigo = $this->security->xss_clean($codigo); 
            $menu = $this->menus->get_data($codigo);

            if($menu)
            {   
                $this->menus->delete($menu->id);
                $code = 200;
                $success = true;

                //Save audit
                //Encode in json format the request
                $model_encode = json_encode($menu);
                $audit = $this->audit->get_data_audit(3,$decode,$model_encode);
                
                $this->audit->save($audit);

            }else{
                $code = 404;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));


        }else{
            show_404();
        }
    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    private function buttons($id)
    {

        if(check_permission("menues_update") || check_permission("menues_delete")){
            
            $option  = '<div class="btn-group m-r-5 m-b-5">
                    <a href="javascript:;" data-toggle="dropdown" class="btn btn-xs btn-warning dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-bars"></i> Acciones
                            <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">';
                        if(check_permission("menues_update")){
                            $option .= '<li >
                                          <a href="'.base_url("app#menues/update/".$id).'" >
                                            <i class="fa fa-edit"> </i>  Editar
                                          </a>
                                        </li>';
                        }    

                        if(check_permission("menues_delete")){
                            $option .= '<li >
                              <a onclick="TableManageMenu.deleteMenu(\''.$id.'\')" href="javascript:;" >
                                <i class="fa fa-trash"> </i>  Eliminar
                              </a>
                            </li>';
                        }     
                    
                    $option .= '
                    </ul>
                </div>';
            return $option;
        }
    }

    /**
     * Get all icons from fa awrsome libs (./assets/plugins/font-awesome/css/font-awesome.css)
     * @return JSON
     **/
    public function get_icons()
    {   
        $fileNM = './assets/plugins/font-awesome/css/font-awesome.css'; 

        $icons = [];
        if (file_exists($fileNM)) {
            $gestor = @fopen($fileNM, 'r');

            if ($gestor) {
                $nl = 0;
                while (($buffer = fgets($gestor, 4096)) !== false) {
                    if ($nl >= 186) {
                        $line = explode(':', $buffer);
                        $name = trim(str_replace('.fa-', '', $line[0]));
                        if ($name != 'content' && $name != '}') {
                            $icons[$name] = $name;
                        }
                    }
                    $nl++;
                }
                fclose($gestor);
                sort($icons);
            }
        }

        $output = array("icons"=>$icons);
            
        $this->output->set_status_header(200)
        ->set_content_type("application/json")
        ->set_output(json_encode($output));

    }


}


