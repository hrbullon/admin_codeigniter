<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Audit extends MY_Controller {

    public function __construct()
	{
		parent::__construct();
        //Check menu permission.
        check_activated_menu('Auditoria');
        //In this controller doesn't loaded the model because it´s loaded with autoload
    }
    
	/**
     *
     * Show Index audit
     * @return View 
     **/
	public function index()
	{
        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('audit/index');
        }
    }
    
    /**
     *
     * Get object list about sessions
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->audit->get_datatables();
        
        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->username;
            $row[] = $item->table;
            $row[] = $item->object;
            $row[] = $item->description;
            $row[] = date('d-m-Y H:i:s', strtotime($item->time));

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->audit->count_all(),
                "recordsFiltered" => $this->audit->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }
    


}

/* End of file Test.php */
/* Location: ./application/controllers/Audit.php */