<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Companies extends MY_Controller {

    //Note Do not desable Empresas menu because may fail Configuration Module    
	public function __construct()
	{
		parent::__construct();

        //Check menu permission
        check_activated_menu('Empresas');
        
        $this->load->model('Companies_model','companies');
        $this->load->model('Countries_model','countries');
        $this->load->model('Taxes_model','taxes');
        $this->load->model('Coins_model','coins');
	}

    /**
     *
     * Show Index com
     * @return View 
     **/
	public function index()
	{
        //Le dice al datatable que oculte la columna de opciones
        if(check_permission("companies_update") || check_permission("companies_delete")){
            $data['option_column'] = 1;
        }else{
            $data['option_column'] = 0;
        }

        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('companies/index',$data);
        }
	}

    /**
     *
     * Get object list about menues
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->companies->get_datatables();

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->social;
            $row[] = $item->fiscal;
            $row[] = $item->local_phone;
            $row[] = $item->local_mobile;

            if($item->state == 1){
                $state = "<span class='label label-success'> Activa </span>";   
            }else{
                $state = "<span class='label label-warning'> Inactiva </span>";
            } 
            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->companies->count_all(),
                "recordsFiltered" => $this->companies->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }
    
    /**
     *
     * Show Create screen
     * @return View 
     **/
    public function create()
    {   
        $data['title'] = "Crear empresa";
        $data['title_header'] = "Empresa"; 
        $data['countries']  = $this->countries->get_all();
        $data['taxes']  = $this->taxes->get_all();
        $data['coins']  = $this->coins->get_all();

        $data['formats'] = get_formats_date();
        $data['zones'] = get_time_zones();
        
        $this->load->view('companies/form',$data);
    }

    /**
     *
     * Show Update screen of company
     * This action is in routes config
     * @return View 
     **/
    public function settings()
    {
        $this->update($this->session->company, true);
    }

    /**
     *
     * Show Update screen
     * @return View 
     **/
    public function update($id, $flag = false)
    {	
    	if($this->input->is_ajax_request())
    	{
            $codigo = $this->security->xss_clean($id); 
    		$company = $this->companies->get_data($codigo);

            if($flag){
                $data['title'] = "Configuración empresa";
                $data['title_header'] = "Configuración";
            }else{
                $data['title'] = "Editar empresa";
                $data['title_header'] = "Empresa";  
            }

    		if($company)
    		{ 
                $data['model'] = $company;
                $data['settings'] = $this->settings->get_data($company->id);
                  
                $data['countries']  = $this->countries->get_all();
                $data['taxes']  = $this->taxes->get_all();
                $data['coins']  = $this->coins->get_all();

                $data['formats'] = get_formats_date();
                $data['zones'] = get_time_zones();
                
    			$this->load->view('companies/form', $data);
    		}else{
    			show_404();
    		}
    	}else{
    		show_404();
    	}
    	
    }

    /**
     *
     * Save data from Role form
     * @param tab string (1,2,3)
     * @return JSON
     **/
    public function save()
    {
    	if($this->input->is_ajax_request())
        {
        	$case = $this->security->xss_clean($this->input->post('tab'));

        	switch ($case) {
        		//Datos de la empresa
        		case '1':
        			$this->save_company();
                    break;
                //Variables de configuración del sistema
        		case '2':
                    $this->save_vars();
        			break;
        		//Configuración del servidor de correo
        		case '3':
        			$this->save_email();
        			break;
        	}
        }else{
            show_404();
        }

    }

    /**
     *
     * Save data from Settings/Tab1 form
     * @param social string
     * @param fiscal string
     * @param email string
     * @param local_phone string
     * @param local_mobile string
     * @param country int
     * @param city string
     * @param address string
     * @return JSON
     **/
    private function save_company()
    {
    	//Reglas para validar el formulario
        $this->form_validation->set_rules('social', 'Razón social', 'required');
        $this->form_validation->set_rules('fiscal', 'RIF', 'required');
        $this->form_validation->set_rules('email', 'Correo electrónico', 'required');
        $this->form_validation->set_rules('local_phone', 'Teléfono local', 'required');
        $this->form_validation->set_rules('local_mobile', 'Teléfono móvil', 'required');

        //Aplicamos las reglas de validación previamente definidas
        if ($this->form_validation->run() == TRUE) {

            $state = $this->security->xss_clean($this->input->post('state'));
        	//Recibo los parametros enviados por el cliente
        	$model['social'] = $this->security->xss_clean($this->input->post('social'));
        	$model['fiscal'] = $this->security->xss_clean($this->input->post('fiscal'));
        	$model['email'] = $this->security->xss_clean($this->input->post('email'));
        	$model['local_phone'] = $this->security->xss_clean($this->input->post('local_phone'));
        	$model['local_mobile'] = $this->security->xss_clean($this->input->post('local_mobile'));
        	$model['country'] = $this->security->xss_clean($this->input->post('country'));
        	$model['city'] = $this->security->xss_clean($this->input->post('city'));
            $model['address'] = $this->security->xss_clean($this->input->post('address'));
            $model['state'] = $this->security->xss_clean($this->input->post('state'));
            
            $codigo = $this->security->xss_clean($this->input->post('company'));

        	if($this->companies->save($model,$codigo))
            {
                $success = true;
                $code = 201;
                $errors = array();

                //Save audit
                //Encode in json format the request
                $model_encode = json_encode($model);
                //Save audit
                //If update or create
                if(!$codigo){
                    $audit = $this->companies->get_data_audit(1,"",$model_encode);
                }else{
                    $audit = $this->companies->get_data_audit(2,$codigo,$model_encode);
                }
                
                $this->audit->save($audit);

            }else{
                
                $success = false;
                $code = 500;
                $errors = array('Error interno en el servidor');
            
            }

        }else{

        	$success = false;
            $code = 200;
            
            $errors = array(
                'social' => form_error('social'),
                'fiscal' => form_error('fiscal'),
                'email' => form_error('email'),
                'local_phone' => form_error('local_phone'),
                'local_mobile' => form_error('local_mobile')
            );
        }

        $output = array("success"=>$success,"errors"=>$errors);
        
        $this->output->set_status_header($code)
        ->set_content_type("application/json")
        ->set_output(json_encode($output));


    }
    
    public function save_vars()
    {
       
    	//Reglas para validar el formulario
        $this->form_validation->set_rules('coin', 'Moneda', 'required');
        $this->form_validation->set_rules('tax', 'Impuesto', 'required');

        //Aplicamos las reglas de validación previamente definidas
        if ($this->form_validation->run() == TRUE) {

        	//Recibo los parametros enviados por el cliente
        	$model['coin'] = $this->security->xss_clean($this->input->post('coin'));
            $model['tax'] = $this->security->xss_clean($this->input->post('tax'));
        	$model['retention'] = $this->security->xss_clean($this->input->post('retention'));
            $model['perception'] = $this->security->xss_clean($this->input->post('perception'));
            $model['format_date'] = $this->security->xss_clean($this->input->post('format_date'));
            $model['time_zone'] = $this->security->xss_clean($this->input->post('time_zone'));
            $model['separator'] = $this->security->xss_clean($this->input->post('separator'));
            $model['decimals'] = $this->security->xss_clean($this->input->post('decimals'));

            $codigo = $this->security->xss_clean($this->input->post('setting'));

        	if($this->settings->save($model,$codigo))
            {
                $success = true;
                $code = 201;
                $errors = array();
                
                //Set time zone
                set_config_item(2,"time_zone",$model['time_zone']);

                //Save audit
                //Encode in json format the request
                $model_encode = json_encode($model);
                //Save audit
                $audit = $this->settings->get_data_audit(2,$codigo,$model_encode);
                $this->audit->save($audit);

            }else{

                $success = false;
                $code = 500;
                $errors = array('Error interno en el servidor');
            
            }

        }else{

        	$success = false;
            $code = 200;
            
            $errors = array(
                'coin' => form_error('coin'),
                'tax' => form_error('tax')
            );
        }

        $output = array("success"=>$success,"errors"=>$errors);
        
        $this->output->set_status_header($code)
        ->set_content_type("application/json")
        ->set_output(json_encode($output));
    }

    /**
     *
     * Save data from Settings/Tab1 form
     * @param protocolo string
     * @param outserver string
     * @param outport string
     * @param out_name string
     * @param out_username string
     * @param out_password string
     * @return JSON
     **/
    public function save_email()
    {
    	//Reglas para validar el formulario
        $this->form_validation->set_rules('protocolo', 'Protocolo', 'required');
        $this->form_validation->set_rules('outserver', 'Servidor', 'required');
        $this->form_validation->set_rules('outport', 'Puerto', 'required');
        $this->form_validation->set_rules('out_name', 'Nombre', 'required');
        $this->form_validation->set_rules('username', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        //Aplicamos las reglas de validación previamente definidas
        if ($this->form_validation->run() == TRUE) {

        	//Recibo los parametros enviados por el cliente
        	$model['protocolo'] = $this->security->xss_clean($this->input->post('protocolo'));
        	$model['outserver'] = $this->security->xss_clean($this->input->post('outserver'));
        	$model['outport'] = $this->security->xss_clean($this->input->post('outport'));
            $model['out_name'] = $this->security->xss_clean($this->input->post('out_name'));
        	$model['out_username'] = $this->security->xss_clean($this->input->post('username'));
        	$model['out_password'] = $this->security->xss_clean($this->input->post('password'));

            $codigo = $this->security->xss_clean($this->input->post('setting'));

        	if($this->settings->save($model,$codigo))
            {
                $success = true;
                $code = 201;
                $errors = array();

                //Save audit
                //Encode in json format the request
                $model_encode = json_encode($model);
                //Save audit
                $audit = $this->settings->get_data_audit(3,$codigo,$model_encode);
                $this->audit->save($audit);

            }else{

                $success = false;
                $code = 500;
                $errors = array('Error interno en el servidor');
            
            }

        }else{

        	$success = false;
            $code = 200;
            
            $errors = array(
                'protocolo' => form_error('protocolo'),
                'outserver' => form_error('outserver'),
                'portserver' => form_error('portserver'),
                'out_name' => form_error('portserver'),
                'out_username' => form_error('out_name'),
                'out_password' => form_error('out_password'),
            );
        }

        $output = array("success"=>$success,"errors"=>$errors);
        
        $this->output->set_status_header($code)
        ->set_content_type("application/json")
        ->set_output(json_encode($output));
    }

    /**
     *
     * Save data from Setitngs - logo form
     * @param logo FILE
     * @return JSON
     **/
    public function save_logo()
    {
        if($this->input->is_ajax_request())
        {   
            //Directory
            $ruta = "uploads/companies/";
            //Get extension of image
            $info = new SplFileInfo($_FILES['logo']['name']);
            $ext = $info->getExtension();
    
            //Check image type
            if (exif_imagetype($_FILES['logo']['tmp_name']) !== IMAGETYPE_GIF &&
                exif_imagetype($_FILES['logo']['tmp_name']) !== IMAGETYPE_JPEG &&
                exif_imagetype($_FILES['logo']['tmp_name']) !== IMAGETYPE_PNG &&
                exif_imagetype($_FILES['logo']['tmp_name']) !== IMG_JPG )
            { 

                $code = 200;
                $success = false;
                $error = "El formato del archivo que intenta subir es inválido";

            }else{

                $codigo = $this->security->xss_clean($this->input->post('setting'));
                $setting_model = $this->settings->get_data($codigo);

                //Should save in audit if user try to upload some file not allowed    
                $model['file'] = $_FILES['logo']['name'];
                $model['extension'] = $ext;

                $fichero = $ruta.md5(date("DmYh-his")).".".$ext;

                if (move_uploaded_file($_FILES['logo']['tmp_name'], $fichero)) {
                    
                    //Update logo in table of database
                    $data['logo'] = $fichero;
                    $this->settings->save($data, $codigo);

                    //Remove file old pictu
                    if(!is_null($setting_model->logo) && !empty($setting_model->logo)){
                        unlink($setting_model->logo);
                    }

                    //Encode in json format the request
                    $model = json_encode($model);
                    //Save audit
                    $audit = $this->settings->get_data_audit(1,$codigo,$model);
                    $this->audit->save($audit);

                    $code = 200;
                    $success = true;
                    $error = "";
                    
                }else{

                    $code = 200;
                    $success = false;
                    $error = "Ocurrió un problema al subir la imagen, intente más tarde!";
    
                }
                
            }

            $output = array("success"=>$success,"error"=>$error);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));
        }
        
    }

    /**
     * Delete some row in the table
     * @param codigo string 
     * @return JSON
     **/
    public function delete($codigo)
    {
        if($this->input->is_ajax_request())
        {
            $codigo = $this->security->xss_clean($codigo); 
            $company = $this->companies->get_data($codigo);

            if($company)
            {   
                $this->company->delete($company->id);
                $code = 200;
                $success = true;

                //Save audit
                //Encode in json format the request
                $model_encode = json_encode($company);
                $audit = $this->audit->get_data_audit(1,$codigo,$model_encode);
                
                $this->audit->save($audit);

            }else{
                $code = 404;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));


        }else{
            show_404();
        }
    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    private function buttons($id)
    {

        if(check_permission("companies_update") || check_permission("companies_delete")){
            
            $option  = '<div class="btn-group m-r-5 m-b-5">
                    <a href="javascript:;" data-toggle="dropdown" class="btn btn-xs btn-warning dropdown-toggle" aria-expanded="false">
                            <i class="fa fa-bars"></i> Acciones
                            <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">';
                        if(check_permission("companies_update")){
                            $option .= '<li >
                                          <a href="'.base_url("app#companies/update/".$id).'" >
                                            <i class="fa fa-edit"> </i>  Editar
                                          </a>
                                        </li>';
                        }    

                        if(check_permission("companies_delete")){
                            $option .= '<li >
                              <a onclick="TableManageCompanies.deleteCompany(\''.$id.'\')" href="javascript:;" >
                                <i class="fa fa-trash"> </i>  Eliminar
                              </a>
                            </li>';
                        }     
                    
                    $option .= '
                    </ul>
                </div>';
            return $option;
        }
    }

}


