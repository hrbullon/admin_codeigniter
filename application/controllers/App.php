<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *  
 **/

class App extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();

	    //User is logued
        //Check session active in table of database
		$session = $this->session->userdata('session_id');
		$inactive = $this->sessions_model->check_active_session($session);	
		
	    //User is logued
	    if(is_null($this->session->userdata('user_id')) || $inactive)
	    {
		   //Destroy session
           $this->session->sess_destroy();
           redirect("auth/login");
        }
    }

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('app/index');
		$this->load->view('layout/footer');
    }
	
}
