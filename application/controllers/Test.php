<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function index()
	{
		$data['username'] = 'hrbullon@gmail.com';
        $data['link'] = base_url('acceso/validate?code='.md5('test'));
        $data['password'] = 'TESTER';
        
        $msg = $this->load->view("emails/recovery_access",$data, TRUE);
      
		//$msg = $this->load->view('emails/new_access', $data, TRUE);
		var_dump(send_email("hrbullon@gmail.com","Restablecer contraseña - SAVICOLA", $msg));
	}

	public function test2(){
    	$archivo = APPPATH. 'config/config.php';
        $contenido = file_get_contents($archivo);
        $lineas = explode("\n", $contenido);

        for($i=0; $i<count($lineas); $i++){

           if(preg_match('/config/', $lineas[$i]) && preg_match('/language/', $lineas[$i])){
               
                $edit = "config['language'] = 'english';";
                //echo "Esta es la variable que quiero editar en al linea " . $lineas[$i];
                file_put_contents($archivo, str_replace($lineas[$i], "$".$edit, $contenido));
           } 
        }
    }


    public function htmlpdf()
    {
        require_once(APPPATH . 'libraries/html2pdf_v4.03/html2pdf.class.php');

     

        ob_start();
        $this->load->view("htmlpdf/test1");
        $html = ob_get_clean();
        //Pasamos esa vista a PDF
        //Le indicamos el tipo de hoja y la codificación de caracteres
        $mipdf = new HTML2PDF('P', 'letter', 'es', 'true', 'UTF-8', array(10, 5, 10, 5));
        $mipdf->pdf->SetDisplayMode('fullpage');
        //Escribimos el contenido en el PDF
        $mipdf->writeHTML($html);
        $mipdf->pdf->IncludeJS('print(TRUE)');
        //Generamos el PDF
        return $mipdf->Output();
        

    }

    public function pdf()
    {
        $this->load->add_package_path( APPPATH . 'third_party/fpdf');
        $this->load->library('pdf');

        $this->pdf = new Pdf();
        $this->pdf->Add_Page('P','A4',0);
        $this->pdf->AliasNbPages();
        $this->pdf->SetFont('Times','',12);
        


        //for($i=1;$i<=40;$i++)
        //    $this->pdf->Cell(0,10,'Imprimiendo línea número '.$i,0,1);
        
        $this->pdf->Output( 'page.pdf' , 'I' );
    }

    public function timezone()
    {
        

        /*$archivo = APPPATH. 'config/config.php';
        $contenido = file_get_contents($archivo);
        $lineas = explode("\n", $contenido);
        $time_zone = "America/Bogotá";

        for($i=0; $i<count($lineas); $i++){

           if(preg_match('/config/', $lineas[$i]) && preg_match('/time_zone/', $lineas[$i])){
               
                $edit = "config['time_zone'] = '".$time_zone."';";
                file_put_contents($archivo, str_replace($lineas[$i], "$".$edit, $contenido));
           } 
        }*/
    }
}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */