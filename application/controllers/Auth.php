<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Auth extends CI_Controller {

	public function __construct() {
		
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->model('Users_model', 'users');
        $this->load->model('Roles_model', 'roles');
        $this->load->model('Codes_model', 'codes');
        $this->load->model('Settings_model', 'settings');

    }

    /**
     *
     * Show login screen
     * @return View 
     **/
    public function login() 
    {   
        //Check session active in table of database
		$session = $this->session->userdata('session_id');
		$inactive = $this->sessions_model->check_active_session($session);	
		
	    //User is logued
	    if(!is_null($this->session->userdata('user_id')) && $inactive) redirect("app");

        //Aplico las reglas de validacion
        $this->form_validation->set_rules('username', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required|callback_check_database');

        $model['username'] = $this->input->post('username');

        if ($this->form_validation->run() == TRUE) {

                $username = $this->security->xss_clean($this->input->post('username'));
                $user = $this->users->get_user_by_username($username);
                $setting = $this->settings->get_data($user->company);
                
                $data = array(
                    "session_id" => 0,
                    "company" => $user->company,
                    "logo_company" => (!empty($setting->logo) && !is_null($setting->logo))? $setting->logo : "",
                    "theme" => (!empty($user->theme) && !is_null($user->theme))? $user->theme : "default",
                    "style_header" => (!empty($user->style_header) && !is_null($user->style_header))? $user->style_header : "navbar-default",
                    "username" => $user->username,
                    "profile" => (!empty($user->profile) && !is_null($user->profile))? $user->profile : "uploads/users-profile/user-icon.png",
                    "roles" => implode(',',$this->roles->get_roles_by_user($user->id)),
                    "user_id" => $user->id,
                    "expira" => time()+$this->config->item('sess_expiration')
                );

                $this->session->set_userdata($data);

                $session = array('user' => $user->id,
                    'ip_address' => get_real_ip(),
                    'start_time' => date('Y-m-d H:i:s'),
                    'end_time' => '',
                    'state' => 1
                );

                $session_id = $this->sessions_model->insert($session);

                //If save session in database
                //Set session_id with the session.
                if($session_id > 0){
                    $this->session->session_id = $session_id;
                    //Encode in json format the request
                    $model = json_encode($model);
                    //Save audit
                    $audit = $this->users->get_data_audit(6,$session_id,$model);
                    $this->audit->save($audit);
                }

                redirect('app');
        }else{

            //If username is incorrect
            if(form_error("username"))
            {
                //Encode in json format the request
                $model = json_encode($model);
                //Save audit
                $audit = $this->users->get_data_audit(4,"",$model);
                $this->audit->save($audit);
            }

            //If password is incorrect
            if(form_error("password"))
            {
                $model['password'] = $this->input->post("password");
                //Encode in json format the request
                $model = json_encode($model);
                //Save audit
                $audit = $this->users->get_data_audit(5,"",$model);
                $this->audit->save($audit);
            }

        }

        $this->load->view('auth/login');
    }

    /**
     *
     * Logout user
     * @param session_id int ['SESSION']
     * @return Redirect to login /
     **/
    public function logout()
    {   
        if($this->sessions_model->finnish($this->session->userdata('session_id')))
        {

            $session_id = $this->session->userdata('session_id');
            //Encode in json format the request
            $model = json_encode($this->session->userdata());
            //Save audit
            $audit = $this->users->get_data_audit(9,$session_id,$model);
            $this->audit->save($audit);

            //Destroy session
            $this->session->sess_destroy();
            redirect("auth/login");
        }
    }

    /**
     *
     * Recovery access
     * @param email string
     * @return View 
     **/
    public function recovery()
    {
        //Check session active in table of database
		$session = $this->session->userdata('session_id');
        $inactive = $this->sessions_model->check_active_session($session);	
        
        //User is logued
        if(!is_null($this->session->userdata('user_id')) && !$inactive) redirect("app");

        if($this->input->method() == 'post')
        {
            $username = $this->security->xss_clean($this->input->post('username'));

            //Aplico las reglas de validacion
            $this->form_validation->set_rules('username', 'Usuario', 'callback_check_username');

            if ($this->form_validation->run() === TRUE) 
            {
                 //Get info about user
                 $user = $this->users->get_user_by_username($username);

                 $code = generate_code(27);
                 $link = base_url('auth/change_password/'.$code);
 
                 $object = array('user' => $user->id, 
                                 'code'  => $code, 
                                 'kind'   => '1', 
                                 'state' => '1');
                 
                 //Save new generated code
                 $this->codes->insert($object);
 
                 //Get email template
                 $data['username'] = $username;
                 $data['link'] = $link;
                 
                 $msg = $this->load->view("emails/recovery_access",$data, TRUE);
                 
                 //Encode in json format the request
                 $model['username'] = $username;
                 $model = json_encode($model);

                 //Send email 
                 if(send_email($username,"Restablecer contraseña - SAVICOLA", $msg))
                 {
                    //Get audit object
                    $audit = $this->users->get_data_audit(11,"",$model);

                    //Set message
                    $this->session->set_flashdata('success', 'Hemos enviado un link para restablecer su contraseña al correo: '.$username);
                 
                    //Clear input post
                    unset($_POST);    

                 }else{
                    //Get audit object
                    $audit = $this->users->get_data_audit(10,$user->id,$model);
                 }
 
                 //Save audit
                 $this->audit->save($audit);
            }

        }
        
        $this->load->view('auth/recovery');
    }

    /**
     *
     * Recovery access
     * @param code string
     * @return View 
     **/
    public function change_password($code)
    {
       //Check session active in table of database
       $session = $this->session->userdata('session_id');
       $inactive = $this->sessions_model->check_active_session($session);	
        
       //User is logued
       if(!is_null($this->session->userdata('user_id')) && !$inactive) redirect("app");

       $model = $this->codes->get_code($code);

       //If is valid send code else send word invalid to view
       $data['code'] = ($model->state == "1")? $code : "invalid";

       //Is a post
       if($this->input->method() == 'post')
       {
            $password = $this->security->xss_clean($this->input->post('password'));
            $password_confirm = $this->security->xss_clean($this->input->post('password_confirm'));

            //Rules to validate form
            $this->form_validation->set_rules('password', 'Contraseña', 'required');
            $this->form_validation->set_rules('password_confirm', 'Confirmar contraseña', 'required|matches[password]');

            if($this->form_validation->run())
            {
                //Get info about user
                $user = $this->users->get_data($model->user);

                //Update password of user
                if($this->users->update_password($model->user, $password_confirm))
                {
                    $data["state"] = 0;
                    //Disable code
                    $this->codes->update($data,$model->id);
                    $model_encode = json_encode($user['user']);

                    //Get audit object
                    $audit = $this->users->get_data_audit(13,$user['user']->id,$model_encode);
 
                    //Save audit
                    $this->audit->save($audit);

                     //Set message
                     $this->session->set_flashdata('success', 'Su contraseña ha sido actualizada!');
                 
                }

            }
       }

       return $this->load->view('auth/change_password', $data);
    }

    public function check_database()
    {
        
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $user = $this->users->get_user_by_username($username);

        if ($user) {
            
            //Verifico el estado del usuario 0 -> inactivo, 1 -> activo, 2 -> bloqueo
            if($user->state == "2")
            {
                $this->form_validation->set_message('check_database', 'Usuario bloqueado temporalmente, por intentos fallídos de acceso');
                return false;
            }

            if($user->state == "0")
            {
                $this->form_validation->set_message('check_database', 'Usuario inactivo, consulte con el administrador del sistema');
                return false;
            }

            //Veriffy if password is correct!
            if(password_verify($password.SALT_HASH,$user->password))
            {
                return true;
            }else{
                $this->form_validation->set_message('check_database', 'Contraseña incorrecta');
                return false;
            }

        }else{
            $this->form_validation->set_message('check_database', 'Usuario incorrecto');
            return false;
        }

    }

    public function check_username()
    {
        
        $username = $this->security->xss_clean($this->input->post('username'));

        //Field username is null or empty
        if(is_null($username) || empty($username))
        {
            $this->form_validation->set_message('check_username', 'El campo usuario es requerido');
            return false;
        }else{
            $user = $this->users->get_user_by_username($username);

            if ($user) {
                
                //Verifico el estado del usuario 0 -> inactivo, 1 -> activo, 2 -> bloqueo
                if($user->state == "2")
                {
                    $this->form_validation->set_message('check_username', 'Usuario bloqueado temporalmente, por intentos fallídos de acceso');
                    return false;
                }

                if($user->state == "0")
                {
                    $this->form_validation->set_message('check_username', 'Usuario inactivo, consulte con el administrador del sistema');
                    return false;
                }

            }else{
               
                //Encode in json format the request
                $model['error'] = "Usuario incorrecto";
                $model['username'] = $username;

                $model = json_encode($model);
                //Save audit
                $audit = $this->users->get_data_audit(12,"",$model);
                $this->audit->save($audit);

                $this->form_validation->set_message('check_username', 'Usuario incorrecto');
                return false;
            }
        }

    }


    

}


