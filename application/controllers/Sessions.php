<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Custom controller extended of CI_Controller
require APPPATH . 'libraries/MY_Controller.php';

/**
 * CodeIgniter Controller Class
 *
 *
 * @package     CodeIgniter
 * @category    Controller
 * @author      Haderson Bullón
 *
    
 **/

class Sessions extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        //Check menu permission.
        check_activated_menu('Sesiones');
        //Load model for this controller
		$this->load->model('Sessions_model','sessions');
	}

    /**
     *
     * Show Index roles
     * @return View 
     **/
	public function index()
	{
        //If request is post
        if( $this->input->method() == 'post' )
        {
            $this->datatable();
        }
        //If reuques is get
        if( $this->input->method() == 'get' )
        {
            $this->load->view('sessions/index');
        }
	}

    /**
     *
     * Get object list about sessions
     * @return JSON 
     **/
	public function datatable()
    {
        $list = $this->sessions->get_datatables();
        //Get info about settings
        $settings = $this->settings->get_data($this->session->company);

        $format = (!is_null($settings->format_date) && !empty($settings->format_date)) ? $settings->format_date : "d-m-Y";

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->username;
            $row[] = $item->ip_address;
            $row[] = date($format." H:m:s", strtotime($item->start_time));

            if($item->end_time !== "0000-00-00 00:00:00"){
            	$fin = date($format." H:m:s", strtotime($item->end_time));	
            }else{
            	$fin = "";
            }  

            $row[] = $fin;

            if($item->state == 1){
                $state = "<span class='label label-success'> Activo </span>";   
            }else{
                $state = "<span class='label label-warning'> Terminada </span>";
            }

            $row[] = $state;
            $row[] = $this->buttons($item->id);

            $data[] = $row;

        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->sessions->count_all(),
                "recordsFiltered" => $this->sessions->count_filtered(),
                "data" => $data,
        );

        //output to json format
        $this->output->set_content_type("application/json")->set_output(json_encode($output));

    }

    /**
     * Get options button for a datatable
     * @return View
     **/
    public function buttons($id)
    {
        return '<a href="app#audit?log_session='.$id.'" class="btn btn-xs btn-info"> <i class="fa fa-eye"></i> Ver </a>';
    }

    /**
     * Close all sessions defferents to session now
     * @return JSON Bolean
     **/
    public function destroy_all_sessions()
    {
        //Verify if user is logged
        if($this->input->is_ajax_request() && !is_null($this->session->userdata('user_id')))
        {
            $active_session = $this->session->session_id;
            
            if( $this->sessions->destroy_all_sessions() ){
                $code = 200;
                $success = true;

                //Save audit
                $audit = $this->sessions->get_data_audit(1,'','');
                $this->audit->save($audit);
            }else{
                $code = 200;
                $success = false;
            }

            $output = array("success"=>$success);
            
            $this->output->set_status_header($code)
            ->set_content_type("application/json")
            ->set_output(json_encode($output));
        }
    }

}
