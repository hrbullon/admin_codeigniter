<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
    
    }

	
    public function forbidden()
    {
    	if($this->input->is_ajax_request()){
    	
    		$this->load->view('errors/html/error_forbidden');	
    	
    	}else{

    		$this->load->view('layout/header');
    		$this->load->view('errors/html/error_forbidden');
    		$this->load->view('layout/footer');
    	
    	}
		
    }
	
	public function desactivated()
    {
    	if($this->input->is_ajax_request()){
			$this->load->view('errors/html/error_desactivated');
		}else{

    		$this->load->view('layout/header');
    		$this->load->view('errors/html/error_desactivated');
    		$this->load->view('layout/footer');
    	
    	}	
    }
}
