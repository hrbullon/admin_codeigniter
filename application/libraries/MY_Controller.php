<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller {
   
   public function __construct()
   {
	    parent::__construct();
		
		//Check session active in table of database
		$session = $this->session->userdata('session_id');
		$inactive = $this->sessions_model->check_active_session($session);

		//User is logued
	    if(is_null($this->session->userdata('user_id')) || $inactive)
	    {
		   //Destroy session
		   $this->session->sess_destroy();

		   //Is active
		   if($inactive){
				//Request is AJAX
				if($this->input->is_ajax_request()){
					$response = $this->output->set_status_header(401)
					  ->set_content_type("application/json")
					  ->set_output(json_encode(array('success'=>FALSE)));
					 
					  echo $response->final_output;exit;
				 }else{
					  redirect('auth/login');
				 }
		   }
	    } 

	    //Get name of controller
	    $controller = $this->router->fetch_class();
	    //Get name method
	    $action = $this->router->fetch_method();

		//Create name of permission to exec any request
		//Example: users_index -> Is user controler and index for a list data
	    $permission = $controller.'_'.$action;
	    
	    $flag = true;
	    $check = 0;

		//Check if user want to close all actives sessions
		if(preg_match('/destroy_all_sessions/', $permission)){
	    	return true;
	    }

	    //User wants to save data from form
	    if(preg_match('/save/', $permission)){
	    	$check = 2;
	    }

	    //in module menu the user can see many icons for assign to some menu
	    if(preg_match('/get_icons/', $permission)){
	    	$check = 3; 
	    }

	    //This request get all permissions of some role and it can assign and revoke a permission
	    if(preg_match('/get_all/', $permission) || preg_match('/asign_revoke/', $permission)){
			$check = 4;
	    }

	    //All users has this permission because is to see his profiles
		if(preg_match('/preferences/', $permission) || preg_match('/profile/', $permission) || preg_match('/update_password/', $permission) || preg_match('/history/', $permission)){
			return true;
		}
		
	    //Save settings of system
	    if($permission == "settings_save" || $permission == "settings_save_logo"){
	    	$check = 5;
	    }

	    switch ($check) {
	    	case 0:
	    			if(!check_permission($permission)){
			    		$flag = false;
			    	}
	    		break;
	    	case 1:
		    		if(!check_permission($controller.'_index')){
			    		$flag = false;
			    	}
	    		break;
	    	case 2:
	    			$permission_cr = $controller.'_create';
		    		$permission_up = $controller.'_update';
		    		
		    		if(!check_permission($permission_cr) || !check_permission($permission_up)){
		    			$flag = false;
		    		}
	    		break;
	    	case 3:
	    			//For get icons you need create and update permission
	    			$permission_cr = $controller.'_create';
		    		$permission_up = $controller.'_update';
		    		
		    		if(!check_permission($permission_cr) || !check_permission($permission_up)){
		    			$flag = false;
		    		}
	    		break;
	    	case 4:
	    			//For get icons you need create and update permission
	    			$permission_in = $controller.'_index';
		    		
		    		if(!check_permission($permission_in)){
		    			$flag = false;
		    		}
		    case 5:
		    		//User should has permissions to save_settings
		    		$permission_in = $controller.'_index';
		    		
		    		if(!check_permission($permission_in)){
		    			$flag = false;
		    		}				
	    		break;			
	    }

	    if(!$flag){
	    	redirect('errors/forbidden');
	    }

   }


}